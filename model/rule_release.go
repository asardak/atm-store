package model

import (
	"encoding/json"

	multierror "github.com/hashicorp/go-multierror"
	"gopkg.in/src-d/go-kallax.v1"
)

type RuleRelease struct {
	kallax.Model      `table:"rule_release" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`

	Rule `kallax:",inline"`
}

func (r *RuleRelease) BeforeSave() error {
	return multierror.Append(
		r.beforeSave(),
		r.Rule.BeforeSave(),
		r.Timestamps.BeforeSave(),
	).ErrorOrNil()
}

func (r *RuleRelease) beforeSave() error {
	if r.ClientID == nil {
		r.NoClient = true
	}
	if r.UtilID == nil {
		r.NoUtil = true
	}
	if r.FirmwareID == nil {
		r.NoFirmware = true
	}

	return nil
}

func (r *RuleRelease) ActionLogProps() ([]byte, error) {
	return json.Marshal(struct {
		ID         int64 `json:"id"`
		PlatformID int64 `json:"platform_id"`
		EnvID      int64 `json:"env_id"`
	}{
		ID:         r.ID,
		PlatformID: r.PlatformID,
		EnvID:      r.EnvID,
	})
}

func (r *RuleRelease) BuildChanges(old *RuleRelease) ([]byte, error) {
	var oldPackage *Package
	if old != nil {
		oldPackage = &old.Package
	}

	diff, err := buildChanges(oldPackage, &r.Package)
	if err != nil {
		return nil, err
	}
	if len(diff) == 0 {
		return nil, nil
	}

	return json.Marshal(diff)
}
