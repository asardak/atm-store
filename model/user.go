package model

import (
	"encoding/json"
	"time"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/src-d/go-kallax.v1"
)

type User struct {
	kallax.Model      `table:"users" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`

	ID           int64      `json:"id"`
	Login        string     `json:"login"`
	Role         Role       `json:"role"`
	PasswordHash string     `json:"-"`
	DeletedAt    *time.Time `json:"deleted_at"`
}

const (
	numBcryptPasses = 10
)

// IsAdmin checks if the user is admin.
func (u User) IsAdmin() bool {
	return u.Role == RoleAdmin
}

// IsDeleted checks if the user is deleted.
func (u User) IsDeleted() bool {
	return u.DeletedAt != nil
}

// IsZero checks if the user is absent.
func (u User) IsZero() bool {
	return u.ID == 0
}

// HasPassword checks if pass is the user's password.
func (u User) HasPassword(pass string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.PasswordHash), []byte(pass)) == nil
}

// SetPassword sets the user's password hash.
func (u *User) SetPassword(pass string) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(pass), numBcryptPasses)
	if err != nil {
		return err
	}

	u.PasswordHash = string(hash)
	return nil
}

type Role int

const (
	RoleNone Role = iota
	RoleUser
	RoleAdmin

	RoleNoneName  = "none"
	RoleUserName  = "user"
	RoleAdminName = "admin"
)

func (r Role) String() string {
	switch r {
	case RoleNone:
		return RoleNoneName
	case RoleUser:
		return RoleUserName
	case RoleAdmin:
		return RoleAdminName
	default:
		return "unknown"
	}
}

func (r *Role) MarshalJSON() ([]byte, error) {
	return json.Marshal(r.String())
}

func (r *Role) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)

	switch s {
	case RoleUserName:
		*r = RoleUser
	case RoleAdminName:
		*r = RoleAdmin
	}

	return err
}
