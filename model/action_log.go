package model

import (
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type ActionLog struct {
	kallax.Model `table:"action_log" pk:"id,autoincr"`
	kallax.Timestamps

	ID         int64   `json:"id"`
	Subject    string  `json:"subject"`
	SubjectID  *int64  `json:"subject_id"`
	ActionType string  `json:"action_type"`
	Props      string  `json:"-"`
	Changes    string  `json:"-"`
	AuthorID   int64   `json:"author_id"`
	AuthorIP   *string `json:"author_ip"`

	Author          *User `kallax:"-" json:"author"`
	*PackageChanges `kallax:"-" json:"package_changes"`
}

type PackageChanges struct {
	Package

	NoClient   *bool `json:"no_client"`
	NoUtil     *bool `json:"no_util"`
	NoFirmware *bool `json:"no_firmware"`
}
