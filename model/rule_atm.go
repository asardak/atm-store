package model

import (
	"encoding/json"

	"reflect"

	multierror "github.com/hashicorp/go-multierror"
	"gopkg.in/src-d/go-kallax.v1"
)

type RuleAtm struct {
	kallax.Model      `table:"rule_atm" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`
	Rule              `kallax:",inline"`

	AtmID int64 `json:"atm_id"`

	Atm *Atm `kallax:"-" json:"atm"`
}

func (r *RuleAtm) BeforeSave() error {
	return multierror.Append(
		r.Rule.BeforeSave(),
		r.Timestamps.BeforeSave(),
	).ErrorOrNil()
}

func (r *RuleAtm) ActionLogProps() ([]byte, error) {
	return json.Marshal(struct {
		PlatformID int64 `json:"platform_id"`
		EnvID      int64 `json:"env_id"`
		AtmID      int64 `json:"atm_id"`
	}{
		PlatformID: r.PlatformID,
		EnvID:      r.EnvID,
		AtmID:      r.AtmID,
	})
}

func (r *RuleAtm) BuildChanges(old *RuleAtm) ([]byte, error) {
	var oldPackage *Package
	if old != nil {
		oldPackage = &old.Package
	}

	diff, err := buildChanges(oldPackage, &r.Package)
	if err != nil {
		return nil, err
	}
	if len(diff) == 0 {
		return nil, nil
	}

	return json.Marshal(diff)
}

const (
	RuleReleaseSubject = "rule_release"
	RuleAtmSubject     = "rule_atm"
	UpdateAction       = "update"
	CreateAction       = "create"
	DeleteAction       = "delete"
)

var packageLogFields = map[string]bool{
	"client_id":   true,
	"util_id":     true,
	"firmware_id": true,
	"no_client":   true,
	"no_util":     true,
	"no_firmware": true,
}

func buildChanges(oldVal, newVal interface{}) (map[string]interface{}, error) {
	oldMap := map[string]interface{}{}
	newMap := map[string]interface{}{}

	var logFields map[string]bool
	switch newVal.(type) {
	case *Package:
		logFields = packageLogFields
	}

	if oldVal != nil {
		oldJSON, err := json.Marshal(oldVal)
		if err != nil {
			return nil, err
		}

		err = json.Unmarshal(oldJSON, &oldMap)
		if err != nil {
			return nil, err
		}
	}

	newJSON, err := json.Marshal(newVal)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(newJSON, &newMap)
	if err != nil {
		return nil, err
	}

	for k, v := range newMap {
		oldVal, oldExist := oldMap[k]
		if oldExist && reflect.DeepEqual(v, oldVal) || !logFields[k] {
			delete(newMap, k)
		}
	}

	return newMap, nil
}
