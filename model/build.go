//go:generate kallax gen

package model

import (
	"path/filepath"
	"strings"

	"fmt"

	"gopkg.in/src-d/go-kallax.v1"
)

type BuildPathSchema int

const (
	Client   = "client"
	Util     = "util"
	Firmware = "firmware"

	BuildPathOld BuildPathSchema = 1
	BuildPathNew BuildPathSchema = 2
)

var validFwFormats = map[string]bool{"": true, "vc": true, "spiimage": true}

type Build struct {
	kallax.Model `table:"builds" pk:"id,autoincr"`
	kallax.Timestamps

	ID             int64  `json:"id"`
	PlatformID     int64  `json:"platform_id"`
	EnvID          int64  `json:"env_id"`
	Version        string `json:"version"`
	VersionNum     int    `json:"version_num"`
	VersionID      string `json:"version_id"`
	Component      string `json:"component"`
	Md5            string `json:"md5"`
	Size           int64  `json:"size"`
	FileName       string `json:"file_name"`
	Description    string `json:"description"`
	ForceDowngrade bool   `json:"force_downgrade"`

	Platform *Platform `kallax:"-" json:"platform"`
	Env      *Env      `kallax:"-" json:"env"`
	URL      string    `kallax:"-" json:"url"`

	ReleaseRules []*RuleRelease `kallax:"-" json:"release_rules"`
	AtmRules     []*RuleAtm     `kallax:"-" json:"atm_rules"`
	NetworkRules []*RuleNetwork `kallax:"-" json:"network_rules"`
}

func (b *Build) IncludeURL(root string) {
	b.URL = root + b.Path()
}

func (b *Build) Path() string {
	return filepath.Join("/", buildDirPath(b), b.FileName)
}

func (b *Build) UpdateAllowed(other *Build) bool {
	if !other.IsVersionValid() {
		return true
	}

	if b.GetVersionID() != other.GetVersionID() {
		return true
	}

	if b.ForceDowngrade && strings.ToLower(b.Version) != strings.ToLower(other.Version) {
		return true
	}

	if other.GetVersionNumber() < b.GetVersionNumber() {
		return true
	}

	return false
}

func (b *Build) GetVersionNumber() int {
	if b.Component == Firmware {
		return FirmwareVersion(b.Version).Number()
	}

	return CommonVersion(b.Version).Number()
}

func (b *Build) GetVersionID() string {
	if b.Component == Firmware {
		return FirmwareVersion(b.Version).ID()
	}

	return CommonVersion(b.Version).ID()
}

func (b *Build) IsVersionValid() bool {
	if b.Component == Firmware {
		return FirmwareVersion(b.Version).Valid()
	}

	return CommonVersion(b.Version).Valid()
}

func (b *Build) BuildFileName() string {
	ver := underscore(b.Version)

	switch b.Component {
	case Client:
		return fmt.Sprintf("client_green_%s.tar.lzma", ver)
	case Util:
		return fmt.Sprintf("videocomfort_%s.tar.lzma", ver)
	case Firmware:
		return fmt.Sprintf("digicap_%s.dav", ver)
	default:
		panic("unknown component")
	}
}

func (b *Build) IsReleased() bool {
	return len(b.ReleaseRules) > 0 ||
		len(b.AtmRules) > 0 ||
		len(b.NetworkRules) > 0
}

func IsComponentValid(c string) bool {
	return map[string]bool{Client: true, Util: true, Firmware: true}[c]
}

func IsFwFormatValid(f string) bool {
	return validFwFormats[f]
}

func buildDirPath(build *Build) string {
	return filepath.Join(build.Platform.Name, build.Env.Name, build.Component, underscore(build.Version))
}

func underscore(s string) string {
	return strings.Join(strings.Split(s, " "), "_")
}
