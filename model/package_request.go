package model

type PackageRequest struct {
	PlatformName    string
	EnvName         string
	SN              string
	IP              string
	ClientVersion   string
	UtilVersion     string
	FirmwareVersion string

	Platform *Platform
	Env      *Env
	Atm      *Atm
}
