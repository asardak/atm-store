package model

import (
	"errors"
	"strings"

	"gopkg.in/src-d/go-kallax.v1"
)

type Atm struct {
	kallax.Model      `table:"atm" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`

	ID         int64   `json:"id"`
	SN         string  `json:"sn"`
	PlatformID *int64  `json:"platform_id"`
	EnvID      *int64  `json:"env_id"`
	IP         *string `json:"ip"`
	MAC        *string `json:"mac"`
}

var ErrBlankSN = errors.New("Serial number cannot be blank")

func (c *Atm) BeforeSave() error {
	err := c.Timestamps.BeforeSave()
	if err != nil {
		return err
	}

	c.SN = strings.ToLower(strings.TrimSpace(c.SN))
	if c.SN == "" {
		return ErrBlankSN
	}
	return nil
}
