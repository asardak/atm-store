package model

import "gopkg.in/src-d/go-kallax.v1"

type Platform struct {
	kallax.Model      `table:"platforms" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`

	ID   int64  `json:"id"`
	Name string `json:"name"`
}
