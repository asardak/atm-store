package model

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type FileStore struct {
	Root string
}

func NewFileStore(root string) *FileStore {
	return &FileStore{Root: root}
}

func (p *FileStore) WriteBuild(build *Build, r io.Reader) error {
	fpath := filepath.Join(p.Root, build.Path())
	dir := filepath.Dir(fpath)
	err := os.MkdirAll(dir, os.ModeDir|0755)
	if err != nil {
		return err
	}

	file, err := os.OpenFile(fpath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	hash := md5.New()
	mw := io.MultiWriter(file, hash)

	size, err := io.Copy(mw, r)
	if err != nil {
		return err
	}

	build.Size = int64(size)
	build.Md5 = fmt.Sprintf("%x", hash.Sum(nil))

	return nil
}
