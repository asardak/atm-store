package model

import (
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type Network struct {
	kallax.Model      `table:"network" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`

	ID          int64   `json:"id"`
	Addr        string  `json:"addr"`
	Description *string `json:"description"`
}
