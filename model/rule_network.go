package model

import (
	multierror "github.com/hashicorp/go-multierror"
	"gopkg.in/src-d/go-kallax.v1"
)

type RuleNetwork struct {
	kallax.Model      `table:"rule_network" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`
	Rule              `kallax:",inline"`

	NetworkID int64 `json:"network_id"`

	Network *Network `kallax:"-" json:"network"`
}

func (r *RuleNetwork) BeforeSave() error {
	return multierror.Append(
		r.Rule.BeforeSave(),
		r.Timestamps.BeforeSave(),
	).ErrorOrNil()
}
