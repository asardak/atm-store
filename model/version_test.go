package model

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type TestVersionStruct struct {
	Version  string
	IdString string
	Number   int
}

func TestFirmwareNumber(t *testing.T) {
	versions := []TestVersionStruct{
		{"5.3.0Build 160405", "build ", 160405},
		{"v5.3.0Build 160405", "build ", 160405},
		{"V5.3.0Build 160405", "build ", 160405},
		{"v0.0.0_id_11-26-33_160704", "_id_11-26-33_", 160704},
		{"V0.0.0_id_11-26-33_160704", "_id_11-26-33_", 160704},
		{"v0.0.0_id_11-26-33_160704876823468", "_id_11-26-33_", 160704876823468},
	}

	Convey("Given valid firmware versions", t, func() {
		for _, v := range versions {
			num := FirmwareVersion(v.Version)
			So(num.ID(), ShouldEqual, v.IdString)
			So(num.Number(), ShouldEqual, v.Number)
			So(num.Valid(), ShouldBeTrue)
		}
	})
}

func TestCommonNumber(t *testing.T) {
	versions := []TestVersionStruct{
		{"v1.2.33d_r0_egestaging_caebbae", "d_r0_egestaging_caebbae", 10233},
		{"V1.2.33d_r0_egestaging_caebbae", "d_r0_egestaging_caebbae", 10233},
		{"v0.9.7", "", 907},
		{"V0.9.7", "", 907},
		{"v1.0.1-1-g40e5016", "-1-g40e5016", 10001},
		{"v0.0.0_id2016-07-01_16-12-39", "_id2016-07-01_16-12-39", 0},
	}

	Convey("Given valid common versions", t, func() {
		for _, v := range versions {
			num := CommonVersion(v.Version)
			So(num.ID(), ShouldEqual, v.IdString)
			So(num.Number(), ShouldEqual, v.Number)
			So(num.Valid(), ShouldBeTrue)
		}
	})
}

func TestInvalidFirmwareNumbers(t *testing.T) {
	versions := []string{
		"5.3.0Build",
		"v5.3.0Build",
		"V5.3.0Build",
		"v0.0.0_id_11-26-33_",
		"V0.0.0",
		"lalala.nanana.olololo",
	}

	Convey("Given invalid firmware versions", t, func() {
		for _, v := range versions {
			num := FirmwareVersion(v)
			So(num.Valid(), ShouldBeFalse)
		}
	})
}

func TestInvalidCommonNumbers(t *testing.T) {
	versions := []string{
		"1.2.33d_r0_egestaging_caebbae",
		"0.9.7",
		"lalala.nanana.olololo",
	}

	Convey("Given invalid common versions", t, func() {
		for _, v := range versions {
			num := CommonVersion(v)
			So(num.Valid(), ShouldBeFalse)
		}
	})
}
