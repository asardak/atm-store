package model

type Package struct {
	ClientID   *int64 `json:"client_id"`
	UtilID     *int64 `json:"util_id"`
	FirmwareID *int64 `json:"firmware_id"`
	NoClient   bool   `json:"no_client"`
	NoUtil     bool   `json:"no_util"`
	NoFirmware bool   `json:"no_firmware"`

	Client   *Build `kallax:"-" json:"client"`
	Util     *Build `kallax:"-" json:"util"`
	Firmware *Build `kallax:"-" json:"firmware"`
}

func (p *Package) Merge(pp *Package) {
	if pp == nil {
		return
	}

	if p.ClientEmpty() {
		p.ClientID = pp.ClientID
		p.Client = pp.Client
		p.NoClient = pp.NoClient

		if pp.Client != nil {
			p.ClientID = &pp.Client.ID
		}
	}

	if p.UtilEmpty() {
		p.UtilID = pp.UtilID
		p.Util = pp.Util
		p.NoUtil = pp.NoUtil

		if pp.Util != nil {
			p.UtilID = &pp.Util.ID
		}
	}

	if p.FirmwareEmpty() {
		p.FirmwareID = pp.FirmwareID
		p.Firmware = pp.Firmware
		p.NoFirmware = pp.NoFirmware

		if pp.Firmware != nil {
			p.FirmwareID = &pp.Firmware.ID
		}
	}
}

func (p *Package) Diff(pp *Package) {
	if p.Client != nil && !p.Client.UpdateAllowed(pp.Client) {
		p.ClientID = nil
		p.Client = nil
	}
	if p.Util != nil && !p.Util.UpdateAllowed(pp.Util) {
		p.UtilID = nil
		p.Util = nil
	}
	if p.Firmware != nil && !p.Firmware.UpdateAllowed(pp.Firmware) {
		p.FirmwareID = nil
		p.Firmware = nil
	}
}

func (p *Package) Add(b *Build) {
	if b == nil {
		return
	}

	if b.Component == Client {
		p.ClientID = &b.ID
		p.Client = b
	}
	if b.Component == Util {
		p.UtilID = &b.ID
		p.Util = b
	}
	if b.Component == Firmware {
		p.FirmwareID = &b.ID
		p.Firmware = b
	}
}

func (p *Package) Completed() bool {
	return !p.ClientEmpty() && !p.UtilEmpty() && !p.FirmwareEmpty()
}

func (p *Package) Empty() bool {
	return p.ClientEmpty() && p.UtilEmpty() && p.FirmwareEmpty()
}

func (p *Package) ClientEmpty() bool {
	return p.ClientID == nil && !p.NoClient
}

func (p *Package) UtilEmpty() bool {
	return p.UtilID == nil && !p.NoUtil
}

func (p *Package) FirmwareEmpty() bool {
	return p.FirmwareID == nil && !p.NoFirmware
}

func (p *Package) Builds() []*Build {
	res := []*Build{}
	if !p.ClientEmpty() {
		res = append(res, p.Client)
	}
	if !p.FirmwareEmpty() {
		res = append(res, p.Firmware)
	}
	if !p.UtilEmpty() {
		res = append(res, p.Util)
	}
	return res
}
