package model

import (
	"gopkg.in/src-d/go-kallax.v1"
)

type Env struct {
	kallax.Model      `table:"envs" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`

	ID   int64  `json:"id"`
	Name string `json:"name"`
}
