package model

import (
	"errors"
	"fmt"
)

type Rule struct {
	ID         int64   `json:"id"`
	PlatformID int64   `json:"platform_id"`
	EnvID      int64   `json:"env_id"`
	Comment    *string `json:"comment"`

	Package `kallax:",inline"`

	Platform *Platform `kallax:"-" json:"platform"`
	Env      *Env      `kallax:"-" json:"env"`
}

var (
	BuildNotIncludedError  = errors.New("Build not included before save")
	BuildIncompatibleError = errors.New("Build incompatible")
)

func (r *Rule) BeforeSave() error {
	if r.NoClient {
		r.ClientID = nil
	}
	if r.NoUtil {
		r.UtilID = nil
	}
	if r.NoFirmware {
		r.FirmwareID = nil
	}
	if r.ClientID == nil {
		r.Client = nil
	}
	if r.UtilID == nil {
		r.Util = nil
	}
	if r.FirmwareID == nil {
		r.Firmware = nil
	}

	if r.ClientID != nil && (r.Client == nil || *r.ClientID != r.Client.ID) ||
		r.UtilID != nil && (r.Util == nil || *r.UtilID != r.Util.ID) ||
		r.FirmwareID != nil && (r.Firmware == nil || *r.FirmwareID != r.Firmware.ID) {
		return BuildNotIncludedError
	}

	if r.Client != nil && (r.Client.PlatformID != r.PlatformID || r.Client.EnvID != r.EnvID || r.Client.Component != Client) ||
		r.Util != nil && (r.Util.PlatformID != r.PlatformID || r.Util.EnvID != r.EnvID || r.Util.Component != Util) ||
		r.Firmware != nil && (r.Firmware.PlatformID != r.PlatformID || r.Firmware.EnvID != r.EnvID || r.Firmware.Component != Firmware) {
		return BuildIncompatibleError
	}

	return nil
}

func (r *Rule) GetRule() *Rule {
	return r
}

type ErrWrongPlatform struct {
	CorrectPlatform *Platform
}

func (err ErrWrongPlatform) Error() string {
	return fmt.Sprintf("expected platform %q", err.CorrectPlatform.Name)
}

type ErrWrongEnv struct {
	CorrectEnv *Env
}

func (err ErrWrongEnv) Error() string {
	return fmt.Sprintf("expected env %q", err.CorrectEnv.Name)
}

type RuleInterface interface {
	GetRule() *Rule
}
