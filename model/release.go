package model

import (
	"gopkg.in/src-d/go-kallax.v1"
)

type Release struct {
	kallax.Model      `table:"releases" pk:"id,autoincr"`
	kallax.Timestamps `json:"-"`

	ID         int64  `json:"id"`
	Component  string `json:"component"`
	BuildID    int64  `json:"build_id"`
	PlatformID int64  `json:"platform_id"`
	EnvID      int64  `json:"env_id"`
	AuthorID   *int64 `json:"author_id"`
	AuthorIP   string `json:"author_ip"`
}
