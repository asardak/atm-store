package model

import (
	"regexp"
	"strconv"
	"strings"
)

type (
	FirmwareVersion string
	CommonVersion   string
)

var (
	FirmwareVersionRe = regexp.MustCompile(`^[vV]?\d+\.\d+\.\d+(.*[^\d])(\d+)$`)
	CommonVersionRe   = regexp.MustCompile(`^[vV](\d+)\.(\d+)\.(\d+)(.+)?$`)
)

type Version interface {
	ID() string
	Number() int
	Valid() bool
	Version() string
}

func NewVersion(version, component string) Version {
	if component == Firmware {
		return FirmwareVersion(version)
	}

	return CommonVersion(version)
}

func (n FirmwareVersion) ID() string {
	matches := FirmwareVersionRe.FindStringSubmatch(string(n))
	if len(matches) < 3 {
		return ""
	}

	return strings.ToLower(matches[1])
}

func (n CommonVersion) ID() string {
	matches := CommonVersionRe.FindStringSubmatch(string(n))
	if len(matches) < 5 {
		return ""
	}

	return strings.ToLower(matches[4])
}

func (n FirmwareVersion) Number() int {
	matches := FirmwareVersionRe.FindStringSubmatch(string(n))
	if len(matches) < 3 {
		return 0
	}

	i, _ := strconv.Atoi(matches[2])

	return i
}

func (n CommonVersion) Number() int {
	matches := CommonVersionRe.FindStringSubmatch(string(n))
	if len(matches) < 5 {
		return 0
	}

	x, _ := strconv.Atoi(matches[1])
	y, _ := strconv.Atoi(matches[2])
	z, _ := strconv.Atoi(matches[3])

	return z + y*100 + x*10000
}

func (n CommonVersion) Valid() bool {
	return len(CommonVersionRe.FindStringSubmatch(string(n))) >= 4
}

func (n FirmwareVersion) Valid() bool {
	return len(FirmwareVersionRe.FindStringSubmatch(string(n))) == 3
}

func (n CommonVersion) Version() string {
	return string(n)
}

func (n FirmwareVersion) Version() string {
	return string(n)
}
