package api

import (
	"net/http"
	"strconv"

	"atm-store/model"
	"atm-store/store"
	"github.com/labstack/echo"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

func (a *API) RuleReleaseList(c echo.Context) error {
	q := model.NewRuleReleaseQuery()
	ruleStore := store.NewRuleReleases(a.db)
	rules, err := ruleStore.FindAll(q)
	if err != nil {
		return err
	}

	if rules == nil {
		rules = []*model.RuleRelease{}
	}

	err = ruleStore.IncludeAll(rules...)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rules,
	})
}

func (a *API) RuleReleaseShow(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	releaseStore := store.NewRuleReleases(a.db)
	rule, err := releaseStore.FindOne(model.NewRuleReleaseQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = releaseStore.IncludeAll(rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleReleaseUpdate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	releaseStore := store.NewRuleReleases(a.db)
	rule, err := releaseStore.FindOne(model.NewRuleReleaseQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = c.Bind(rule)
	if err != nil {
		return err
	}

	err = releaseStore.IncludeAll(rule)
	if err != nil {
		return err
	}

	err = releaseStore.LogSave(c, rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleReleaseCreate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	rule := &model.RuleRelease{}
	err := c.Bind(rule)
	if err != nil {
		return err
	}

	ruleStore := store.NewRuleReleases(a.db)
	_, err = ruleStore.FindOne(model.NewRuleReleaseQuery().Where(
		kallax.And(
			kallax.Eq(model.Schema.RuleRelease.PlatformID, rule.PlatformID),
			kallax.Eq(model.Schema.RuleRelease.EnvID, rule.EnvID),
		),
	))
	if err == nil {
		return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
			"errors": []map[string]string{{
				"field": "platform_id",
				"msg":   "Правило для этой пары платформа/окружение уже задано",
			}, {
				"field": "env_id",
				"msg":   "Правило для этой пары платформа/окружение уже задано",
			}},
		})
	}
	if err != nil && err != kallax.ErrNotFound {
		return err
	}

	err = ruleStore.IncludeAll(rule)
	if err != nil {
		return err
	}

	err = ruleStore.LogSave(c, rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleReleaseDelete(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if !currentUser.IsAdmin() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rules := store.NewRuleReleases(a.db)
	rule, err := rules.FindOne(model.NewRuleReleaseQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = rules.LogDelete(c, rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]bool{"deleted": true})
}
