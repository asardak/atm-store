package api

import (
	"net/http"
	"strconv"
	"time"

	"atm-store/model"
	"github.com/AlekSi/pointer"
	"github.com/labstack/echo"
	"gopkg.in/src-d/go-kallax.v1"
)

func (a *API) UserList(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if !currentUser.IsAdmin() {
		return errForbidden
	}

	userStore := model.NewUserStore(a.db)
	users, err := userStore.FindAll(model.NewUserQuery().Order(kallax.Asc(model.Schema.User.Login)))
	if err != nil {
		return err
	}
	if users == nil {
		users = []*model.User{}
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": users,
	})
}

func (a *API) UserCreate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if !currentUser.IsAdmin() {
		return errForbidden
	}

	body := struct {
		Login                string     `json:"login"`
		Role                 model.Role `json:"role"`
		Password             string     `json:"password"`
		PasswordConfirmation string     `json:"password_confirmation"`
	}{}

	err := c.Bind(&body)
	if err != nil {
		return err
	}

	if body.Password == "" {
		return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
			"errors": []map[string]string{{
				"field": "password",
				"msg":   "Пароль не может быть пустым",
			}},
		})
	}

	if body.Password != body.PasswordConfirmation {
		return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
			"errors": []map[string]string{{
				"field": "password",
				"msg":   "Введенные пароли не совпадают",
			}},
		})
	}

	user := &model.User{
		Login: body.Login,
		Role:  body.Role,
	}

	err = user.SetPassword(body.Password)
	if err != nil {
		return err
	}

	userStore := model.NewUserStore(a.db)
	err = userStore.Insert(user)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user,
	})
}

func (a *API) UserUpdate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if !currentUser.IsAdmin() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	userStore := model.NewUserStore(a.db)
	user, err := userStore.FindOne(model.NewUserQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	body := struct {
		Login                string     `json:"login"`
		Role                 model.Role `json:"role"`
		Password             string     `json:"password"`
		PasswordConfirmation string     `json:"password_confirmation"`
	}{}

	err = c.Bind(&body)
	if err != nil {
		return err
	}

	if body.Password != body.PasswordConfirmation {
		return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
			"errors": []map[string]string{{
				"field": "password",
				"msg":   "Введенные пароли не совпадают",
			}},
		})
	}

	user.Login = body.Login
	user.Role = body.Role
	if body.Password != "" {
		err = user.SetPassword(body.Password)
		if err != nil {
			return err
		}
	}

	_, err = userStore.Update(user)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user,
	})
}

func (a *API) UserDelete(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if !currentUser.IsAdmin() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	userStore := model.NewUserStore(a.db)
	user, err := userStore.FindOne(model.NewUserQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	user.DeletedAt = pointer.ToTime(time.Now())
	_, err = userStore.Update(user)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user,
	})
}

func (a *API) UserUndelete(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if !currentUser.IsAdmin() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	userStore := model.NewUserStore(a.db)
	user, err := userStore.FindOne(model.NewUserQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	user.DeletedAt = nil
	_, err = userStore.Update(user)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user,
	})
}
