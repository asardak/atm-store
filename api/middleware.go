package api

import (
	"errors"
	"net"
	"net/http"
	"net/url"
	"strings"

	"atm-store/model"

	"github.com/labstack/echo"
	"gopkg.in/src-d/go-kallax.v1"
)

var errUserNotFound = errors.New("userID not found")

func (api *API) clientAuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if !strings.HasPrefix(c.Request().URL.Path, "/client") {
			return next(c)
		}

		c.Set("currentUser", &model.User{})
		login, pass, ok := c.Request().BasicAuth()
		if !ok {
			return next(c)
		}

		userStore := model.NewUserStore(api.db)
		user, err := userStore.FindOne(model.NewUserQuery().FindByLogin(login))
		if err != nil {
			return err
		}

		if user.IsDeleted() {
			return errForbidden
		}

		if !user.HasPassword(pass) {
			return errBadLogin
		}

		c.Set("currentUser", user)
		return next(c)
	}
}

func (f *API) faceAuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if !strings.HasPrefix(c.Request().URL.Path, "/face") {
			return next(c)
		}

		c.Set("currentUser", &model.User{})
		cookie, err := c.Cookie(authCookieName)
		if err != nil && err != http.ErrNoCookie {
			return err
		}

		if err == http.ErrNoCookie {
			return next(c)
		}

		cookVal, _ := url.QueryUnescape(cookie.Value)
		data := map[string]interface{}{}
		err = f.secureCookie.Decode(authCookieName, cookVal, &data)
		if err != nil {
			return err
		}

		id, ok := data["userID"].(int)
		if !ok {
			return errUserNotFound
		}

		userStore := model.NewUserStore(f.db)
		user, err := userStore.FindOne(model.NewUserQuery().FindByID(int64(id)))
		if err == kallax.ErrNotFound {
			return next(c)
		}
		if err != nil {
			return err
		}

		if user.IsDeleted() {
			return errForbidden
		}

		c.Set("currentUser", user)
		return next(c)
	}
}

func ipMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var ip string
		r := c.Request()
		if realIP := r.Header.Get("X-Real-IP"); realIP != "" {
			ip = realIP
		} else {
			var err error
			ip, _, err = net.SplitHostPort(r.RemoteAddr)
			if err != nil {
				return err
			}
		}

		c.Set("currentIP", ip)
		return next(c)
	}
}
