package api

import (
	"net/http"
	"net/url"

	"atm-store/model"
	"github.com/labstack/echo"
)

const (
	authCookieName   = "Util-Auth"
	authCookieMaxAge = 7 * 24 * 60 * 60 // seconds
)

var (
	errForbidden = echo.NewHTTPError(http.StatusForbidden, "forbidden")
	errBadLogin  = echo.NewHTTPError(http.StatusForbidden, "login or password incorrect")
)

func (a *API) CurrentUserShow(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": currentUser,
	})
}

func (f *API) SessionCreate(c echo.Context) error {
	body := struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}{}

	err := c.Bind(&body)
	if err != nil {
		return err
	}

	if body.Login == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "login can't be empty")
	}

	userStore := model.NewUserStore(f.db)
	user, err := userStore.FindOne(model.NewUserQuery().FindByLogin(body.Login))
	if err != nil {
		return err
	}

	if user == nil {
		return err
	}

	if user.IsDeleted() {
		return errForbidden
	}

	if body.Password == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "password can't be empty")
	}

	if !user.HasPassword(body.Password) {
		return errBadLogin
	}

	data := map[string]interface{}{
		"userID": int(user.ID),
	}
	encData, err := f.secureCookie.Encode(authCookieName, data)
	if err != nil {
		return err
	}

	c.SetCookie(&http.Cookie{
		Name:     authCookieName,
		Value:    url.QueryEscape(encData),
		MaxAge:   authCookieMaxAge,
		Path:     "/",
		Domain:   "",
		Secure:   false,
		HttpOnly: false,
	})

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user,
	})
}

func (f *API) SessionDelete(c echo.Context) error {
	c.SetCookie(&http.Cookie{
		Name:     authCookieName,
		Value:    "",
		MaxAge:   -1,
		Path:     "/",
		Domain:   "",
		Secure:   false,
		HttpOnly: false,
	})

	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "ok",
	})
}
