package api

import (
	"net/http"
	"strings"

	"atm-store/model"
	"atm-store/store"

	"github.com/labstack/echo"
)

var (
	ErrInvalidComponentName = echo.NewHTTPError(http.StatusUnprocessableEntity, "Component name invalid")
	ErrBlankPlatform        = echo.NewHTTPError(http.StatusUnprocessableEntity, "Platform can't be blank")
	ErrBlankEnv             = echo.NewHTTPError(http.StatusUnprocessableEntity, "Env can't be blank")
	ErrBlankComponent       = echo.NewHTTPError(http.StatusUnprocessableEntity, "Component can't be blank")
	ErrBlankVersion         = echo.NewHTTPError(http.StatusUnprocessableEntity, "Version can't be blank")
	ErrBlankSignature       = echo.NewHTTPError(http.StatusUnprocessableEntity, "Signature can't be blank")
	ErrInvalidVersionNumber = echo.NewHTTPError(http.StatusUnprocessableEntity, "Version number invalid")
	ErrVersionReleased      = echo.NewHTTPError(http.StatusUnprocessableEntity, "Version already released")
	ErrFileBlank            = echo.NewHTTPError(http.StatusUnprocessableEntity, "File can't be blank")
)

func (a *API) GetUpdates(c echo.Context) error {
	pr := &model.PackageRequest{
		PlatformName:    strings.ToLower(c.Param("platform")),
		EnvName:         strings.ToLower(c.Param("env")),
		SN:              strings.ToLower(c.QueryParam("sn")),
		ClientVersion:   strings.ToLower(c.QueryParam("client_version")),
		UtilVersion:     strings.ToLower(c.QueryParam("util_version")),
		FirmwareVersion: strings.ToLower(c.QueryParam("firmware_version")),
		IP:              c.Get("currentIP").(string),
	}

	packs := store.NewPackages(a.db)
	pack, err := packs.FindPackage(pr)
	if err != nil {
		return err
	}

	pack.Diff(&model.Package{
		Client:   &model.Build{Component: model.Client, Version: pr.ClientVersion},
		Util:     &model.Build{Component: model.Util, Version: pr.UtilVersion},
		Firmware: &model.Build{Component: model.Firmware, Version: pr.FirmwareVersion},
	})

	buildStore := store.NewBuilds(a.db)
	err = buildStore.IncludeAll(pack.Client, pack.Util, pack.Firmware)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, a.newRespStruct(pack.Builds()...))
}

type respStruct struct {
	ClientVersion   string `json:"client-version,omitempty"`
	ClientUrl       string `json:"client-url,omitempty"`
	UtilVersion     string `json:"util-version,omitempty"`
	UtilUrl         string `json:"util-url,omitempty"`
	FirmwareVersion string `json:"fw-version,omitempty"`
	FirmwareUrl     string `json:"fw-url,omitempty"`
}

func (a *API) newRespStruct(builds ...*model.Build) *respStruct {
	bs := respStruct{}

	for _, b := range builds {
		if b == nil {
			continue
		}

		switch b.Component {
		case model.Client:
			bs.ClientVersion = b.Version
			bs.ClientUrl = a.publicRoot + b.Path()

		case model.Util:
			bs.UtilVersion = b.Version
			bs.UtilUrl = a.publicRoot + b.Path()

		case model.Firmware:
			bs.FirmwareVersion = b.Version
			bs.FirmwareUrl = a.publicRoot + b.Path()
		}
	}

	return &bs
}
