package api

import (
	"net/http"
	"strconv"
	"strings"

	"atm-store/model"
	"github.com/labstack/echo"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

func (a *API) NetworkList(c echo.Context) error {
	store := model.NewNetworkStore(a.db)
	networks, err := store.FindAll(model.NewNetworkQuery().Order(kallax.Asc(model.Schema.Network.CreatedAt)))
	if err != nil {
		return err
	}

	if networks == nil {
		networks = []*model.Network{}
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": networks,
	})
}

func (a *API) NetworkCreate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	network := &model.Network{}
	err := c.Bind(network)
	if err != nil {
		return err
	}

	store := model.NewNetworkStore(a.db)
	err = store.Insert(network)
	if err != nil {
		if strings.Contains(err.Error(), "invalid input syntax for type inet") {
			return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
				"errors": []map[string]string{{
					"field": "addr",
					"msg":   "Неправильный формат адреса",
				}},
			})
		}
		return err
	}

	// Запрашиваем подсеть, чтобы получить адрес в нормальном виде
	network, err = store.FindOne(model.NewNetworkQuery().FindByID(network.ID))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": network,
	})
}

func (a *API) Network(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	store := model.NewNetworkStore(a.db)
	network, err := store.FindOne(model.NewNetworkQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": network,
	})
}

func (a *API) NetworkUpdate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	store := model.NewNetworkStore(a.db)
	network, err := store.FindOne(model.NewNetworkQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = c.Bind(network)
	if err != nil {
		return err
	}

	_, err = store.Update(network)
	if err != nil {
		return err
	}

	// Запрашиваем подсеть, чтобы получить адрес в нормальном виде
	network, err = store.FindOne(model.NewNetworkQuery().FindByID(network.ID))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": network,
	})
}

func (a *API) NetworkDelete(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	netID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	netStore := model.NewNetworkStore(a.db)
	net, err := netStore.FindOne(model.NewNetworkQuery().FindByID(int64(netID)))
	if err != nil {
		return err
	}

	err = netStore.Delete(net)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "ok",
	})
}
