package api

import (
	"net/http"
	"strconv"

	"atm-store/store"
	"atm-store/model"
	"github.com/labstack/echo"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

func (a *API) RuleAtmList(c echo.Context) error {
	page := 1
	limit := 100
	var err error

	if c.QueryParam("page") != "" {
		page, err = strconv.Atoi(c.QueryParam("page"))
		if err != nil {
			return err
		}
	}

	if c.QueryParam("per_page") != "" {
		limit, err = strconv.Atoi(c.QueryParam("per_page"))
		if err != nil {
			return err
		}
	}

	q := model.NewRuleAtmQuery()
	if c.QueryParam("q") != "" {
		camStore := model.NewAtmStore(a.db)
		camsQuery := model.NewAtmQuery().Where(kallax.Like(model.Schema.Atm.SN, c.QueryParam("q")+"%"))
		cams, err := camStore.FindAll(camsQuery)
		if err != nil {
			return err
		}

		camIDs := []interface{}{}
		for _, c := range cams {
			camIDs = append(camIDs, c.ID)
		}

		q = q.Where(kallax.In(model.Schema.RuleAtm.AtmID, camIDs...))
	}

	currentUser := c.Get("currentUser").(*model.User)
	if c.QueryParam("my") == "true" && !currentUser.IsZero() {
		logStore := model.NewActionLogStore(a.db)
		logs, err := logStore.FindAll(
			model.NewActionLogQuery().
				FindByAuthorID(kallax.Eq, int64(currentUser.ID)).
				FindBySubject(model.RuleAtmSubject),
		)
		if err != nil {
			return err
		}

		ruleIDs := []interface{}{}
		for _, l := range logs {
			ruleIDs = append(ruleIDs, l.SubjectID)
		}

		q = q.Where(kallax.In(model.Schema.RuleAtm.ID, ruleIDs...))
	}

	ruleAtms := store.NewRuleAtms(a.db)
	cnt, err := ruleAtms.Count(q)
	if err != nil {
		return err
	}

	offset := uint64((page - 1) * limit)
	q = q.Order(kallax.Desc(model.Schema.RuleAtm.CreatedAt)).Offset(offset).Limit(uint64(limit))

	rules, err := ruleAtms.FindAll(q)
	if err != nil {
		return err
	}
	if rules == nil {
		rules = []*model.RuleAtm{}
	}

	err = ruleAtms.IncludeAll(rules...)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"pagination": map[string]int64{
			"per_page": int64(limit),
			"current":  int64(page),
			"total":    cnt,
		},
		"data": rules,
	})
}

func (a *API) RuleAtm(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rules := store.NewRuleAtms(a.db)
	rule, err := rules.FindOne(model.NewRuleAtmQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = rules.IncludeAll(rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleAtmUpdate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	ruleAtms := store.NewRuleAtms(a.db)
	rule, err := ruleAtms.FindOne(model.NewRuleAtmQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = c.Bind(rule)
	if err != nil {
		return err
	}

	err = ruleAtms.IncludeAll(rule)
	if err != nil {
		return err
	}

	err = ruleAtms.LogSave(c, rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleAtmCreate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	req := &struct {
		model.RuleAtm
		SN string `json:"sn"`
	}{}

	err := c.Bind(req)
	if err != nil {
		return err
	}

	camStore := model.NewAtmStore(a.db)
	cam, err := camStore.FindOne(model.NewAtmQuery().FindBySN(req.SN))
	if err != nil && err != kallax.ErrNotFound {
		return err
	}

	ruleStore := model.NewRuleAtmStore(a.db)
	if cam != nil {
		_, err = ruleStore.FindOne(model.NewRuleAtmQuery().FindByAtmID(kallax.Eq, cam.ID))
		if err != nil && err != kallax.ErrNotFound {
			return err
		}
		if err == nil {
			return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
				"errors": []map[string]string{{
					"field": "sn",
					"msg":   "Правило для этой камеры уже создано",
				}},
			})
		}
	}

	rule := &req.RuleAtm
	err = camStore.Transaction(func(txCamStore *model.AtmStore) error {
		if cam == nil {
			cam = &model.Atm{
				SN:         req.SN,
				PlatformID: &req.PlatformID,
				EnvID:      &req.EnvID,
			}

			err = txCamStore.Insert(cam)
			if err != nil {
				return err
			}
		}

		rule.AtmID = cam.ID
		txRuleStore := store.InitRuleAtms(txCamStore.Store)
		err = txRuleStore.IncludeAll(rule)
		if err != nil {
			return err
		}

		return txRuleStore.LogSave(c, rule)
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleAtmDelete(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	rules := model.NewRuleAtmStore(a.db)
	rule, err := rules.FindOne(model.NewRuleAtmQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = rules.Delete(rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]bool{"deleted": true})
}
