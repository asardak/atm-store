package api

import (
	"net/http"
	"strconv"

	"atm-store"
	"atm-store/model"
	"atm-store/store"
	"github.com/labstack/echo"
	"gopkg.in/src-d/go-kallax.v1"
)

func (a *API) EnvList(c echo.Context) error {
	store := model.NewEnvStore(a.db)
	envs, err := store.FindAll(model.NewEnvQuery().Order(kallax.Asc(model.Schema.Env.Name)))
	if err != nil {
		return err
	}

	if envs == nil {
		envs = []*model.Env{}
	}

	return c.JSON(http.StatusOK, envs)
}

func (a *API) PlatformList(c echo.Context) error {
	store := model.NewPlatformStore(a.db)
	platforms, err := store.FindAll(model.NewPlatformQuery().Order(kallax.Asc(model.Schema.Platform.Name)))
	if err != nil {
		return err
	}

	if platforms == nil {
		platforms = []*model.Platform{}
	}

	return c.JSON(http.StatusOK, platforms)
}

func (a *API) AtmList(c echo.Context) error {
	store := model.NewAtmStore(a.db)
	q := model.NewAtmQuery().Limit(1000)

	if c.QueryParam("sn") != "" {
		q = q.Where(kallax.Like(model.Schema.Atm.SN, c.QueryParam("sn")+"%"))
	}

	list, err := store.FindAll(q)
	if err != nil {
		return err
	}

	if list == nil {
		list = []*model.Atm{}
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": list,
	})
}

func (a *API) ActionLogList(c echo.Context) error {
	id, err := strconv.Atoi(c.QueryParam("id"))
	if err != nil {
		return err
	}

	logStore := store.NewActionLogs(a.db)
	actionLogs, err := logStore.FindBySubject(c.QueryParam("subject"), int64(id))
	if err != nil {
		return err
	}

	if actionLogs == nil {
		actionLogs = []*model.ActionLog{}
	}

	err = logStore.IncludeAll(actionLogs...)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": actionLogs,
	})
}

func (a *API) Status(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]interface{}{
		"version": atmstore.Version,
	})
}

func parsePagination(c echo.Context) (uint64, uint64, uint64, error) {
	page := 1
	limit := 100
	var err error

	if c.QueryParam("page") != "" {
		page, err = strconv.Atoi(c.QueryParam("page"))
		if err != nil {
			return 0, 0, 0, err
		}
	}

	if c.QueryParam("per_page") != "" {
		limit, err = strconv.Atoi(c.QueryParam("per_page"))
		if err != nil {
			return 0, 0, 0, err
		}
	}

	offset := uint64((page - 1) * limit)
	return uint64(page), uint64(limit), offset, nil
}
