package api

import (
	"database/sql"
	"expvar"
	"log"
	"net/http"
	"net/http/pprof"
	"runtime"

	"atm-store"

	"github.com/GeertJohan/go.rice"
	"github.com/gorilla/securecookie"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// API обрабатывает запросы к HTTP API
type API struct {
	*echo.Echo

	db           *sql.DB
	secureCookie *securecookie.SecureCookie
	publicRoot   string
	binRoot      string
}

var (
	front = rice.MustFindBox("../spa/dist")
)

func init() {
	expvar.NewString("go_version").Set(runtime.Version())
	expvar.NewString("version").Set(atmstore.Version)
	expvar.Publish("num_goroutine", expvar.Func(func() interface{} {
		return runtime.NumGoroutine()
	}))
}

// New инициализирует HTTP API
func New(db *sql.DB, publicRoot, binRoot string, hashKey, blockKey []byte) *API {
	a := &API{
		Echo:         echo.New(),
		db:           db,
		secureCookie: securecookie.New(hashKey, blockKey),
		publicRoot:   publicRoot,
		binRoot:      binRoot,
	}

	a.HTTPErrorHandler = a.errorHandler

	a.Use(middleware.Logger())
	a.Use(middleware.Recover())
	a.Use(a.clientAuthMiddleware)
	a.Use(a.faceAuthMiddleware)
	a.Use(ipMiddleware)

	a.GET("/debug/vars", echo.WrapHandler(expvar.Handler()))
	a.GET("/debug/panic", func(echo.Context) error { panic("Test panic") })
	a.GET("/debug/pprof", echo.WrapHandler(http.HandlerFunc(pprof.Index)))
	a.GET("/debug/pprof/goroutine", echo.WrapHandler(http.HandlerFunc(pprof.Index)))
	a.GET("/debug/pprof/heap", echo.WrapHandler(http.HandlerFunc(pprof.Index)))
	a.GET("/debug/pprof/cmdline", echo.WrapHandler(http.HandlerFunc(pprof.Cmdline)))
	a.GET("/debug/pprof/profile", echo.WrapHandler(http.HandlerFunc(pprof.Profile)))
	a.GET("/debug/pprof/symbol", echo.WrapHandler(http.HandlerFunc(pprof.Symbol)))
	a.GET("/debug/pprof/trace", echo.WrapHandler(http.HandlerFunc(pprof.Trace)))

	// Assets
	a.GET("/dist/*", echo.WrapHandler(http.StripPrefix("/dist/", http.FileServer(front.HTTPBox()))))

	// Build binaries
	a.Static("/bin", a.binRoot)

	// Web Face
	a.GET("/", redirect("/face/strategy/release"))
	a.GET("/face", redirect("/face/strategy/release"))
	a.GET("/face/", redirect("/face/strategy/release"))
	a.GET("/face/stages", redirect("/face/strategy/release"))

	// SPA
	a.GET("/face/*", spa())

	// Face API
	a.GET("/face/api/current_user", a.CurrentUserShow)
	a.POST("/face/api/session", a.SessionCreate)
	a.DELETE("/face/api/session", a.SessionDelete)

	a.GET("/face/api/rule_release", a.RuleReleaseList)
	a.GET("/face/api/rule_release/:id", a.RuleReleaseShow)
	a.POST("/face/api/rule_release/:id", a.RuleReleaseUpdate)
	a.POST("/face/api/rule_release", a.RuleReleaseCreate)
	a.DELETE("/face/api/rule_release/:id", a.RuleReleaseDelete)

	a.GET("/face/api/rule_atm", a.RuleAtmList)
	a.GET("/face/api/rule_atm/:id", a.RuleAtm)
	a.POST("/face/api/rule_atm/:id", a.RuleAtmUpdate)
	a.POST("/face/api/rule_atm", a.RuleAtmCreate)
	a.DELETE("/face/api/rule_atm/:id", a.RuleAtmDelete)

	a.GET("/face/api/rule_network", a.RuleNetworkList)
	a.GET("/face/api/rule_network/:id", a.RuleNetwork)
	a.POST("/face/api/rule_network", a.RuleNetworkCreate)
	a.POST("/face/api/rule_network/:id", a.RuleNetworkUpdate)
	a.DELETE("/face/api/rule_network/:id", a.RuleNetworkDelete)

	a.GET("/face/api/network", a.NetworkList)
	a.POST("/face/api/network", a.NetworkCreate)
	a.GET("/face/api/network/:id", a.Network)
	a.POST("/face/api/network/:id", a.NetworkUpdate)
	a.DELETE("/face/api/network/:id", a.NetworkDelete)

	a.GET("/face/api/builds", a.BuildList)
	a.GET("/face/api/builds/:id", a.BuildShow)
	a.POST("/face/api/builds/:id", a.BuildUpdate)
	a.DELETE("/face/api/builds/:id", a.BuildDelete)

	a.GET("/face/api/user", a.UserList)
	a.POST("/face/api/user", a.UserCreate)
	a.POST("/face/api/user/:id", a.UserUpdate)
	a.DELETE("/face/api/user/:id", a.UserDelete)
	a.POST("/face/api/user/:id/undelete", a.UserUndelete)

	a.GET("/face/api/atm", a.AtmList)
	a.GET("/face/api/status", a.Status)
	a.GET("/face/api/platforms", a.PlatformList)
	a.GET("/face/api/envs", a.EnvList)
	a.GET("/face/api/action_log", a.ActionLogList)

	// Public API
	a.GET("/updates/:platform/:env", a.GetUpdates)
	a.POST("/upload/:platform/:env", a.UploadBuild)

	return a
}

// Listen запускает HTTP-сервер, addr содержит адрес вида host:port или :port
func (a *API) Listen(addr string) error {
	return a.Echo.Start(addr)
}

func (a *API) errorHandler(err error, c echo.Context) {
	httpErr, isHTTPErr := err.(*echo.HTTPError)
	code := http.StatusInternalServerError
	if isHTTPErr {
		code = httpErr.Code
	}

	err = c.JSON(code, map[string]string{
		"error": err.Error(),
	})

	if err != nil {
		log.Printf("(echo.Context).JSON() failed: %v", err)
	}
}

func redirect(path string) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.Redirect(302, path)
	}
}

func spa() echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.HTML(http.StatusOK, front.MustString("index.html"))
	}
}
