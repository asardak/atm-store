package api

import (
	"net/http"
	"strconv"

	"mime"
	"strings"

	"atm-store/model"
	"atm-store/store"
	"github.com/labstack/echo"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

func (a *API) BuildList(c echo.Context) error {
	conditions := []kallax.Condition{}

	if c.QueryParam("component") != "" {
		conditions = append(conditions, kallax.Eq(model.Schema.Build.Component, c.QueryParam("component")))
	}

	if c.QueryParam("env_id") != "" {
		envID, err := strconv.Atoi(c.QueryParam("env_id"))
		if err != nil {
			return err
		}
		conditions = append(conditions, kallax.Eq(model.Schema.Build.EnvID, envID))
	}

	if c.QueryParam("platform_id") != "" {
		platformID, err := strconv.Atoi(c.QueryParam("platform_id"))
		if err != nil {
			return err
		}
		conditions = append(conditions, kallax.Eq(model.Schema.Build.PlatformID, platformID))
	}

	q := model.NewBuildQuery()
	if len(conditions) > 0 {
		q = q.Where(kallax.And(conditions...))
	}

	builds := store.NewBuilds(a.db)
	total, err := builds.Count(q)
	if err != nil {
		return err
	}

	sortOrder := kallax.Desc
	sortField := model.Schema.Build.CreatedAt
	switch c.QueryParam("sort_field") {
	case "created_at":
		sortField = model.Schema.Build.CreatedAt
	case "version":
		sortField = model.Schema.Build.VersionNum
	}

	switch c.QueryParam("sort_order") {
	case "desc":
		sortOrder = kallax.Desc
	case "asc":
		sortOrder = kallax.Asc
	}

	page, limit, offset, err := parsePagination(c)
	q = q.Order(sortOrder(sortField)).Limit(limit).Offset(offset)
	items, err := builds.FindAll(q)
	if err != nil {
		return err
	}
	if items == nil {
		items = []*model.Build{}
	}

	err = builds.IncludeAll(items...)
	if err != nil {
		return err
	}

	store.IncudeBuildURL(a.publicRoot, items...)

	return c.JSON(http.StatusOK, map[string]interface{}{
		"pagination": map[string]uint64{
			"per_page": limit,
			"page":     page,
			"total":    uint64(total),
		},
		"data": items,
	})
}

func (a *API) BuildShow(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	builds := store.NewBuilds(a.db)
	build, err := builds.FindOne(model.NewBuildQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = builds.IncludeAll(build)
	if err != nil {
		return err
	}

	err = builds.IncludeRules(build)
	if err != nil {
		return err
	}

	store.IncudeBuildURL(a.publicRoot, build)

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": build,
	})
}

func (a *API) BuildUpdate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	builds := store.NewBuilds(a.db)
	build, err := builds.FindOne(model.NewBuildQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	params := struct {
		Description    string `json:"description"`
		ForceDowngrade bool   `json:"force_downgrade"`
	}{}

	err = c.Bind(&params)
	if err != nil {
		return err
	}

	build.Description = params.Description
	build.ForceDowngrade = params.ForceDowngrade
	_, err = builds.Save(build)
	if err != nil {
		return err
	}

	err = builds.IncludeAll(build)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": build,
	})
}

func (a *API) BuildDelete(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	store := model.NewBuildStore(a.db)
	build, err := store.FindOne(model.NewBuildQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = store.Delete(build)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"status": "ok",
	})
}

func (a *API) UploadBuild(c echo.Context) error {
	p, err := getUploadParams(c)
	if err != nil {
		return err
	}

	var build *model.Build
	dbStore := kallax.NewStore(a.db)
	err = dbStore.Transaction(func(txStore *kallax.Store) error {
		platforms := store.InitPlatforms(txStore)
		platform, err := platforms.FindOrCreate(p.Platform)
		if err != nil {
			return err
		}

		envs := store.InitEnvs(txStore)
		env, err := envs.FindOrCreate(p.Env)
		if err != nil {
			return err
		}

		builds := store.InitBuilds(txStore)
		build, err = builds.FindOne(model.NewBuildQuery().
			FindByPlatformID(kallax.Eq, platform.ID).
			FindByEnvID(kallax.Eq, env.ID).
			FindByComponent(p.Component).
			FindByVersion(p.Version),
		)
		if err == kallax.ErrNotFound {
			build = &model.Build{
				PlatformID: platform.ID,
				EnvID:      env.ID,
				Component:  p.Component,
				Version:    p.Version,
				Env:        env,
				Platform:   platform,
			}
		} else if err != nil {
			return err
		} else if err == nil {
			err = builds.IncludeRules(build)
			if err != nil {
				return err
			}
			if build.IsReleased() {
				return ErrVersionReleased
			}

			err = builds.IncludeAll(build)
			if err != nil {
				return err
			}
		}

		build.Description = p.Description

		file, header, err := c.Request().FormFile("build")
		if err != nil {
			return err
		}

		build.FileName = header.Filename
		if file == nil {
			return ErrFileBlank
		}

		fs := model.NewFileStore(a.binRoot)
		err = fs.WriteBuild(build, file)
		if err != nil {
			return err
		}

		_, err = builds.Save(build)
		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, a.newRespStruct(build))
}

type uploadParams struct {
	Platform    string
	Env         string
	Component   string
	Version     string
	Description string
}

func getUploadParams(c echo.Context) (*uploadParams, error) {
	const maxModelLen = 128
	p := &uploadParams{
		Platform:    strings.ToLower(c.Param("platform")),
		Env:         strings.ToLower(c.Param("env")),
		Component:   strings.ToLower(c.QueryParam("component")),
		Version:     strings.ToLower(c.QueryParam("version")),
		Description: c.QueryParam("description"),
	}

	switch {
	case p.Platform == "":
		return nil, ErrBlankPlatform
	case p.Env == "":
		return nil, ErrBlankEnv
	case p.Component == "":
		return nil, ErrBlankComponent
	case !model.IsComponentValid(p.Component):
		return nil, ErrInvalidComponentName
	case p.Version == "":
		return nil, ErrBlankVersion
	case !model.NewVersion(p.Version, p.Component).Valid():
		return nil, ErrInvalidVersionNumber
	}

	return p, nil
}

func isMultipartForm(r *http.Request) bool {
	v := r.Header.Get("Content-Type")
	if v == "" {
		return false
	}
	d, _, err := mime.ParseMediaType(v)
	if err != nil || d != "multipart/form-data" {
		return false
	}

	return true
}
