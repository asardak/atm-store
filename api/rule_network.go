package api

import (
	"net/http"
	"strconv"

	"atm-store/model"
	"atm-store/store"
	"github.com/labstack/echo"
	"gopkg.in/src-d/go-kallax.v1"
)

func (a *API) RuleNetworkList(c echo.Context) error {
	q := model.NewRuleNetworkQuery()
	if c.QueryParam("network_id") != "" {
		modelID, err := strconv.Atoi(c.QueryParam("network_id"))
		if err != nil {
			return err
		}

		q = q.FindByNetworkID(kallax.Eq, int64(modelID))
	}

	ruleNetworks := store.NewRuleNetworks(a.db)
	rules, err := ruleNetworks.FindAll(q)
	if err != nil {
		return err
	}

	if rules == nil {
		rules = []*model.RuleNetwork{}
	}

	err = ruleNetworks.IncludeAll(rules...)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rules,
	})
}

func (a *API) RuleNetwork(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	ruleNetworks := store.NewRuleNetworks(a.db)
	rule, err := ruleNetworks.FindOne(model.NewRuleNetworkQuery().FindByID(int64(id)))
	if err != nil {
		return err
	}

	err = ruleNetworks.IncludeAll(rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleNetworkCreate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	rule := &model.RuleNetwork{}
	err := c.Bind(rule)
	if err != nil {
		return err
	}

	ruleNetworks := store.NewRuleNetworks(a.db)
	err = ruleNetworks.IncludeAll(rule)
	if err != nil {
		return err
	}

	err = ruleNetworks.Insert(rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleNetworkUpdate(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	ruleID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	ruleNetworks := store.NewRuleNetworks(a.db)
	rule, err := ruleNetworks.FindOne(model.NewRuleNetworkQuery().FindByID(int64(ruleID)))
	if err != nil {
		return err
	}

	err = c.Bind(rule)
	if err != nil {
		return err
	}

	err = ruleNetworks.IncludeAll(rule)
	if err != nil {
		return err
	}

	_, err = ruleNetworks.Update(rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": rule,
	})
}

func (a *API) RuleNetworkDelete(c echo.Context) error {
	currentUser := c.Get("currentUser").(*model.User)
	if currentUser.IsZero() {
		return errForbidden
	}

	ruleID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	store := model.NewRuleNetworkStore(a.db)
	rule, err := store.FindOne(model.NewRuleNetworkQuery().FindByID(int64(ruleID)))
	if err != nil {
		return err
	}

	err = store.Delete(rule)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, map[string]interface{}{"status": "ok"})
}
