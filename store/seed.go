package store

import (
	"net/url"

	"database/sql"

	"atm-store"
)

func Seed(cfg *atmstore.Config) error {
	db, err := sql.Open("postgres", cfg.Postgres.URL)
	if err != nil {
		return err
	}

	err = db.Ping()
	if err != nil {
		url, err := url.Parse(cfg.Postgres.URL)
		if err != nil {
			return err
		}

		db, err = sql.Open("postgres", "sslmode=disable")
		if err != nil {
			return err
		}

		_, err = db.Exec("CREATE DATABASE " + url.Path[1:])
		if err != nil {
			return err
		}

		err = db.Close()
		if err != nil {
			return err
		}

		db, err = sql.Open("postgres", cfg.Postgres.URL)
		if err != nil {
			return err
		}
	}

	err = Migrate(db)
	if err != nil {
		return err
	}

	_, err = db.Exec(seedData)
	return err
}

var seedData = `
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'admin', 2, '$2a$10$HNQEaqZ.YwBkWfzG4WQnGOAIkl8ivhWX5PbdUhxRbIZHJwGOgwAle', '2018-05-18 16:58:53.560833', '2018-05-18 16:58:53.560833', NULL);


--
-- Data for Name: action_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO action_log VALUES (1, 'rule_release', 1, 'create', '{"id": 1, "env_id": 27, "platform_id": 28}', '{"no_util": false, "util_id": 113, "client_id": 117, "no_client": false, "firmware_id": 109, "no_firmware": false}', 1, '::1', '2018-05-18 17:04:20.254372', '2018-05-18 17:04:20.254373');
INSERT INTO action_log VALUES (2, 'rule_release', 2, 'create', '{"id": 2, "env_id": 26, "platform_id": 27}', '{"no_util": false, "util_id": 90, "client_id": 94, "no_client": false, "firmware_id": 86, "no_firmware": false}', 1, '::1', '2018-05-18 17:04:50.55174', '2018-05-18 17:04:50.55174');
INSERT INTO action_log VALUES (3, 'rule_release', 3, 'create', '{"id": 3, "env_id": 25, "platform_id": 26}', '{"no_util": false, "util_id": 44, "client_id": 48, "no_client": false, "firmware_id": 40, "no_firmware": false}', 1, '::1', '2018-05-18 17:05:14.344453', '2018-05-18 17:05:14.344453');
INSERT INTO action_log VALUES (4, 'rule_atm', 1, 'create', '{"atm_id": 1, "env_id": 26, "platform_id": 27}', '{"no_util": false, "util_id": 91, "client_id": 96, "no_client": false, "firmware_id": null, "no_firmware": false}', 1, '::1', '2018-05-19 09:07:14.358933', '2018-05-19 09:07:14.358933');
INSERT INTO action_log VALUES (5, 'rule_atm', 2, 'create', '{"atm_id": 2, "env_id": 26, "platform_id": 27}', '{"no_util": false, "util_id": 92, "client_id": null, "no_client": true, "firmware_id": 88, "no_firmware": false}', 1, '::1', '2018-05-19 09:08:01.673349', '2018-05-19 09:08:01.673349');


--
-- Name: action_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('action_log_id_seq', 5, true);


--
-- Data for Name: envs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO envs VALUES (25, 'test', '2018-05-18 17:01:07.512609', '2018-05-18 17:01:07.512609');
INSERT INTO envs VALUES (26, 'prod', '2018-05-18 17:01:35.693914', '2018-05-18 17:01:35.693914');
INSERT INTO envs VALUES (27, 'demo', '2018-05-18 17:01:52.75757', '2018-05-18 17:01:52.75757');


--
-- Data for Name: platforms; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO platforms VALUES (25, 'ubuntu18', '2018-05-18 17:01:07.510893', '2018-05-18 17:01:07.510893');
INSERT INTO platforms VALUES (26, 'win7', '2018-05-18 17:02:06.091862', '2018-05-18 17:02:06.091862');
INSERT INTO platforms VALUES (27, 'win10', '2018-05-18 17:02:41.427603', '2018-05-18 17:02:41.427603');
INSERT INTO platforms VALUES (28, 'centos7', '2018-05-18 17:03:13.97976', '2018-05-18 17:03:13.97976');


--
-- Data for Name: atm; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO atm VALUES (1, '3315777791', NULL, NULL, 27, 26, '2018-05-19 09:07:14.353614', '2018-05-19 09:07:14.353613');
INSERT INTO atm VALUES (2, '233581192', NULL, NULL, 27, 26, '2018-05-19 09:08:01.668429', '2018-05-19 09:08:01.668429');


--
-- Name: atm_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('atm_id_seq', 2, true);


--
-- Name: atms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('atms_id_seq', 2, true);


--
-- Data for Name: builds; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO builds VALUES (1, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.51615', '2018-05-18 17:01:07.516151');
INSERT INTO builds VALUES (2, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.54738', '2018-05-18 17:01:07.547381');
INSERT INTO builds VALUES (3, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.573166', '2018-05-18 17:01:07.573166');
INSERT INTO builds VALUES (4, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.604855', '2018-05-18 17:01:07.604855');
INSERT INTO builds VALUES (5, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.635627', '2018-05-18 17:01:07.635627');
INSERT INTO builds VALUES (6, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.662996', '2018-05-18 17:01:07.662996');
INSERT INTO builds VALUES (7, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.693301', '2018-05-18 17:01:07.693301');
INSERT INTO builds VALUES (8, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.722229', '2018-05-18 17:01:07.722229');
INSERT INTO builds VALUES (9, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.752968', '2018-05-18 17:01:07.752968');
INSERT INTO builds VALUES (10, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.782713', '2018-05-18 17:01:07.782713');
INSERT INTO builds VALUES (11, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.807708', '2018-05-18 17:01:07.807708');
INSERT INTO builds VALUES (12, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 25, '2018-05-18 17:01:07.839142', '2018-05-18 17:01:07.839142');
INSERT INTO builds VALUES (13, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.695747', '2018-05-18 17:01:35.695747');
INSERT INTO builds VALUES (14, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.719624', '2018-05-18 17:01:35.719624');
INSERT INTO builds VALUES (15, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.74336', '2018-05-18 17:01:35.74336');
INSERT INTO builds VALUES (16, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.772412', '2018-05-18 17:01:35.772412');
INSERT INTO builds VALUES (17, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.797522', '2018-05-18 17:01:35.797522');
INSERT INTO builds VALUES (18, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.826207', '2018-05-18 17:01:35.826208');
INSERT INTO builds VALUES (19, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.851634', '2018-05-18 17:01:35.851634');
INSERT INTO builds VALUES (20, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.879694', '2018-05-18 17:01:35.879694');
INSERT INTO builds VALUES (21, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.905892', '2018-05-18 17:01:35.905892');
INSERT INTO builds VALUES (22, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.935457', '2018-05-18 17:01:35.935457');
INSERT INTO builds VALUES (23, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.963745', '2018-05-18 17:01:35.963745');
INSERT INTO builds VALUES (24, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 26, '2018-05-18 17:01:35.989864', '2018-05-18 17:01:35.989864');
INSERT INTO builds VALUES (25, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.759684', '2018-05-18 17:01:52.759684');
INSERT INTO builds VALUES (26, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.785584', '2018-05-18 17:01:52.785584');
INSERT INTO builds VALUES (27, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.8139', '2018-05-18 17:01:52.8139');
INSERT INTO builds VALUES (28, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.839027', '2018-05-18 17:01:52.839027');
INSERT INTO builds VALUES (29, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.869445', '2018-05-18 17:01:52.869445');
INSERT INTO builds VALUES (30, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.900572', '2018-05-18 17:01:52.900572');
INSERT INTO builds VALUES (31, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.926731', '2018-05-18 17:01:52.926731');
INSERT INTO builds VALUES (32, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.953474', '2018-05-18 17:01:52.953474');
INSERT INTO builds VALUES (33, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:52.978748', '2018-05-18 17:01:52.978749');
INSERT INTO builds VALUES (34, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:53.00807', '2018-05-18 17:01:53.00807');
INSERT INTO builds VALUES (35, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:53.03316', '2018-05-18 17:01:53.03316');
INSERT INTO builds VALUES (36, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 25, 27, '2018-05-18 17:01:53.059373', '2018-05-18 17:01:53.059373');
INSERT INTO builds VALUES (37, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.094212', '2018-05-18 17:02:13.465296');
INSERT INTO builds VALUES (38, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.120925', '2018-05-18 17:02:13.494853');
INSERT INTO builds VALUES (39, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.147722', '2018-05-18 17:02:13.523368');
INSERT INTO builds VALUES (40, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.171895', '2018-05-18 17:02:13.552247');
INSERT INTO builds VALUES (41, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.201529', '2018-05-18 17:02:13.588925');
INSERT INTO builds VALUES (42, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.231313', '2018-05-18 17:02:13.624016');
INSERT INTO builds VALUES (43, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.258286', '2018-05-18 17:02:13.650719');
INSERT INTO builds VALUES (44, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.283449', '2018-05-18 17:02:13.685527');
INSERT INTO builds VALUES (45, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.310764', '2018-05-18 17:02:13.714472');
INSERT INTO builds VALUES (46, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.33887', '2018-05-18 17:02:13.742412');
INSERT INTO builds VALUES (47, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.364446', '2018-05-18 17:02:13.768807');
INSERT INTO builds VALUES (48, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 25, '2018-05-18 17:02:06.390048', '2018-05-18 17:02:13.801137');
INSERT INTO builds VALUES (49, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.069675', '2018-05-18 17:02:18.069675');
INSERT INTO builds VALUES (50, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.093987', '2018-05-18 17:02:18.093987');
INSERT INTO builds VALUES (51, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.144087', '2018-05-18 17:02:18.144087');
INSERT INTO builds VALUES (52, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.168997', '2018-05-18 17:02:18.168997');
INSERT INTO builds VALUES (53, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.195688', '2018-05-18 17:02:18.195688');
INSERT INTO builds VALUES (54, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.222358', '2018-05-18 17:02:18.222358');
INSERT INTO builds VALUES (55, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.250387', '2018-05-18 17:02:18.250387');
INSERT INTO builds VALUES (56, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.275811', '2018-05-18 17:02:18.275811');
INSERT INTO builds VALUES (57, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.30466', '2018-05-18 17:02:18.30466');
INSERT INTO builds VALUES (58, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.331608', '2018-05-18 17:02:18.331608');
INSERT INTO builds VALUES (59, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.358816', '2018-05-18 17:02:18.358817');
INSERT INTO builds VALUES (60, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 26, '2018-05-18 17:02:18.384748', '2018-05-18 17:02:18.384748');
INSERT INTO builds VALUES (61, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.757836', '2018-05-18 17:02:30.757836');
INSERT INTO builds VALUES (62, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.784475', '2018-05-18 17:02:30.784475');
INSERT INTO builds VALUES (63, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.814501', '2018-05-18 17:02:30.814501');
INSERT INTO builds VALUES (64, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.841835', '2018-05-18 17:02:30.841835');
INSERT INTO builds VALUES (65, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.871201', '2018-05-18 17:02:30.871201');
INSERT INTO builds VALUES (66, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.916099', '2018-05-18 17:02:30.916099');
INSERT INTO builds VALUES (67, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.949496', '2018-05-18 17:02:30.949496');
INSERT INTO builds VALUES (68, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:30.981402', '2018-05-18 17:02:30.981402');
INSERT INTO builds VALUES (69, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:31.007138', '2018-05-18 17:02:31.007138');
INSERT INTO builds VALUES (70, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:31.033639', '2018-05-18 17:02:31.03364');
INSERT INTO builds VALUES (71, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:31.059014', '2018-05-18 17:02:31.059014');
INSERT INTO builds VALUES (72, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 26, 27, '2018-05-18 17:02:31.09194', '2018-05-18 17:02:31.091941');
INSERT INTO builds VALUES (73, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.429811', '2018-05-18 17:02:41.429812');
INSERT INTO builds VALUES (74, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.456674', '2018-05-18 17:02:41.456674');
INSERT INTO builds VALUES (75, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.482588', '2018-05-18 17:02:41.482589');
INSERT INTO builds VALUES (76, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.5097', '2018-05-18 17:02:41.5097');
INSERT INTO builds VALUES (77, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.535988', '2018-05-18 17:02:41.535988');
INSERT INTO builds VALUES (78, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.56461', '2018-05-18 17:02:41.56461');
INSERT INTO builds VALUES (79, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.591831', '2018-05-18 17:02:41.591831');
INSERT INTO builds VALUES (80, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.622927', '2018-05-18 17:02:41.622927');
INSERT INTO builds VALUES (81, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.650369', '2018-05-18 17:02:41.650369');
INSERT INTO builds VALUES (82, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.678244', '2018-05-18 17:02:41.678244');
INSERT INTO builds VALUES (83, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.703106', '2018-05-18 17:02:41.703107');
INSERT INTO builds VALUES (84, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 25, '2018-05-18 17:02:41.730427', '2018-05-18 17:02:41.730427');
INSERT INTO builds VALUES (85, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:48.821347', '2018-05-18 17:02:48.821347');
INSERT INTO builds VALUES (86, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:48.845458', '2018-05-18 17:02:48.845458');
INSERT INTO builds VALUES (87, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:48.869911', '2018-05-18 17:02:48.869911');
INSERT INTO builds VALUES (88, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:48.898613', '2018-05-18 17:02:48.898613');
INSERT INTO builds VALUES (89, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:48.924687', '2018-05-18 17:02:48.924687');
INSERT INTO builds VALUES (90, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:48.951268', '2018-05-18 17:02:48.951268');
INSERT INTO builds VALUES (91, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:48.976101', '2018-05-18 17:02:48.976101');
INSERT INTO builds VALUES (92, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:49.002904', '2018-05-18 17:02:49.002904');
INSERT INTO builds VALUES (93, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:49.028628', '2018-05-18 17:02:49.028628');
INSERT INTO builds VALUES (94, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:49.0557', '2018-05-18 17:02:49.055701');
INSERT INTO builds VALUES (95, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:49.081529', '2018-05-18 17:02:49.081529');
INSERT INTO builds VALUES (96, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 26, '2018-05-18 17:02:49.107539', '2018-05-18 17:02:49.107539');
INSERT INTO builds VALUES (97, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.185438', '2018-05-18 17:02:57.185439');
INSERT INTO builds VALUES (98, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.209241', '2018-05-18 17:02:57.209242');
INSERT INTO builds VALUES (99, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.239828', '2018-05-18 17:02:57.239828');
INSERT INTO builds VALUES (100, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.264923', '2018-05-18 17:02:57.264923');
INSERT INTO builds VALUES (101, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.295827', '2018-05-18 17:02:57.295828');
INSERT INTO builds VALUES (102, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.323186', '2018-05-18 17:02:57.323186');
INSERT INTO builds VALUES (103, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.349543', '2018-05-18 17:02:57.349543');
INSERT INTO builds VALUES (104, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.376544', '2018-05-18 17:02:57.376544');
INSERT INTO builds VALUES (105, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.403362', '2018-05-18 17:02:57.403362');
INSERT INTO builds VALUES (106, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.430946', '2018-05-18 17:02:57.430946');
INSERT INTO builds VALUES (107, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.457603', '2018-05-18 17:02:57.457603');
INSERT INTO builds VALUES (108, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 27, 27, '2018-05-18 17:02:57.48431', '2018-05-18 17:02:57.48431');
INSERT INTO builds VALUES (110, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.011289', '2018-05-18 17:03:36.102399');
INSERT INTO builds VALUES (111, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.036469', '2018-05-18 17:03:36.136408');
INSERT INTO builds VALUES (112, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.065983', '2018-05-18 17:03:36.165787');
INSERT INTO builds VALUES (113, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.091311', '2018-05-18 17:03:36.19235');
INSERT INTO builds VALUES (114, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.117626', '2018-05-18 17:03:36.219209');
INSERT INTO builds VALUES (115, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.144607', '2018-05-18 17:03:36.246209');
INSERT INTO builds VALUES (116, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.169673', '2018-05-18 17:03:36.280163');
INSERT INTO builds VALUES (117, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.197691', '2018-05-18 17:03:36.308042');
INSERT INTO builds VALUES (118, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.22339', '2018-05-18 17:03:36.339268');
INSERT INTO builds VALUES (121, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.077771', '2018-05-18 17:03:22.077771');
INSERT INTO builds VALUES (122, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.102613', '2018-05-18 17:03:22.102614');
INSERT INTO builds VALUES (123, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.130164', '2018-05-18 17:03:22.130164');
INSERT INTO builds VALUES (124, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.155839', '2018-05-18 17:03:22.155839');
INSERT INTO builds VALUES (125, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.182815', '2018-05-18 17:03:22.182815');
INSERT INTO builds VALUES (126, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.208718', '2018-05-18 17:03:22.208718');
INSERT INTO builds VALUES (127, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.238791', '2018-05-18 17:03:22.238791');
INSERT INTO builds VALUES (128, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.273301', '2018-05-18 17:03:22.273301');
INSERT INTO builds VALUES (129, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.300078', '2018-05-18 17:03:22.300078');
INSERT INTO builds VALUES (130, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.327832', '2018-05-18 17:03:22.327832');
INSERT INTO builds VALUES (131, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.352486', '2018-05-18 17:03:22.352486');
INSERT INTO builds VALUES (132, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 25, '2018-05-18 17:03:22.379549', '2018-05-18 17:03:22.379549');
INSERT INTO builds VALUES (133, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:28.972442', '2018-05-18 17:03:28.972442');
INSERT INTO builds VALUES (134, 'v1.0.2_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.007616', '2018-05-18 17:03:29.007616');
INSERT INTO builds VALUES (135, 'v1.0.3_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.040224', '2018-05-18 17:03:29.040224');
INSERT INTO builds VALUES (136, 'v1.0.4_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.065859', '2018-05-18 17:03:29.065859');
INSERT INTO builds VALUES (137, 'v1.0.1_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.094082', '2018-05-18 17:03:29.094082');
INSERT INTO builds VALUES (138, 'v1.0.2_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.123213', '2018-05-18 17:03:29.123214');
INSERT INTO builds VALUES (139, 'v1.0.3_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.149697', '2018-05-18 17:03:29.149697');
INSERT INTO builds VALUES (140, 'v1.0.4_build001', 0, '', 'util', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.178778', '2018-05-18 17:03:29.178778');
INSERT INTO builds VALUES (141, 'v1.0.1_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.204227', '2018-05-18 17:03:29.204227');
INSERT INTO builds VALUES (142, 'v1.0.2_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.238941', '2018-05-18 17:03:29.238941');
INSERT INTO builds VALUES (143, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.264241', '2018-05-18 17:03:29.264241');
INSERT INTO builds VALUES (144, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 26, '2018-05-18 17:03:29.295547', '2018-05-18 17:03:29.295547');
INSERT INTO builds VALUES (109, 'v1.0.1_build001', 0, '', 'firmware', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:13.982656', '2018-05-18 17:03:36.069827');
INSERT INTO builds VALUES (119, 'v1.0.3_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.252468', '2018-05-18 17:03:36.369221');
INSERT INTO builds VALUES (120, 'v1.0.4_build001', 0, '', 'client', '988fb3c874c807b4724266ebecdd884c', 24, 'build', '', false, 28, 27, '2018-05-18 17:03:14.277561', '2018-05-18 17:03:36.397893');


--
-- Name: builds_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('builds_id_seq', 144, true);


--
-- Name: envs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('envs_id_seq', 27, true);


--
-- Data for Name: network; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO network VALUES (1, '91.84.30.123/10', 'Офис Альфабанк', '2018-05-18 17:06:05.326618', '2018-05-18 17:06:05.326618');
INSERT INTO network VALUES (2, '81.75.0.0/10', 'Офис Газпрома', '2018-05-19 09:04:45.544499', '2018-05-19 09:04:45.544499');


--
-- Name: network_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('network_id_seq', 2, true);


--
-- Name: platforms_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('platforms_id_seq', 28, true);


--
-- Data for Name: releases; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: releases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('releases_id_seq', 1, false);


--
-- Data for Name: rule_atm; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rule_atm VALUES (1, 96, 91, NULL, '2018-05-19 09:07:14.35693', '2018-05-19 09:07:14.35693', false, false, false, 27, 26, NULL, 1);
INSERT INTO rule_atm VALUES (2, NULL, 92, 88, '2018-05-19 09:08:01.671636', '2018-05-19 09:08:01.671636', true, false, false, 27, 26, NULL, 2);


--
-- Data for Name: rule_network; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rule_network VALUES (3, 26, 26, 1, 58, 55, 49, false, false, false, NULL, '2018-05-19 09:04:04.318154', '2018-05-19 09:04:04.318154');
INSERT INTO rule_network VALUES (4, 27, 26, 2, 96, 90, 87, false, false, false, NULL, '2018-05-19 09:05:02.222729', '2018-05-19 09:05:02.222729');
INSERT INTO rule_network VALUES (5, 26, 26, 2, 58, 54, 49, false, false, false, NULL, '2018-05-19 09:05:15.940016', '2018-05-19 09:05:15.940016');
INSERT INTO rule_network VALUES (6, 27, 27, 1, 108, 104, 100, false, false, false, NULL, '2018-05-19 09:06:04.854627', '2018-05-19 09:06:04.854627');
INSERT INTO rule_network VALUES (7, 28, 27, 1, 119, 115, NULL, false, false, false, NULL, '2018-05-19 09:06:24.625622', '2018-05-19 09:06:24.625622');
INSERT INTO rule_network VALUES (2, 27, 26, 1, NULL, NULL, 86, true, false, false, NULL, '2018-05-19 09:03:50.486354', '2018-05-19 09:06:37.868187');


--
-- Name: rule_network_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rule_network_id_seq', 7, true);


--
-- Data for Name: rule_release; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO rule_release VALUES (1, 28, 27, 117, 113, 109, false, false, false, NULL, '2018-05-18 17:04:20.251791', '2018-05-18 17:04:20.251791');
INSERT INTO rule_release VALUES (2, 27, 26, 94, 90, 86, false, false, false, NULL, '2018-05-18 17:04:50.550051', '2018-05-18 17:04:50.550051');
INSERT INTO rule_release VALUES (3, 26, 25, 48, 44, 40, false, false, false, NULL, '2018-05-18 17:05:14.343301', '2018-05-18 17:05:14.343301');


--
-- Name: rule_release_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rule_release_id_seq', 3, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- PostgreSQL database dump complete
--
`
