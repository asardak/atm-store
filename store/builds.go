package store

import (
	"database/sql"

	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type Builds struct {
	*model.BuildStore
}

func NewBuilds(db *sql.DB) *Builds {
	return &Builds{model.NewBuildStore(db)}
}

func InitBuilds(store *kallax.Store) *Builds {
	return &Builds{&model.BuildStore{Store: store}}
}

func (s *Builds) IncludeAll(items ...*model.Build) error {
	err := s.IncludePlatform(items...)
	if err != nil {
		return err
	}

	err = s.IncludeEnv(items...)
	if err != nil {
		return err
	}

	return nil
}

func (s *Builds) IncludePlatform(items ...*model.Build) error {
	items = compactBuilds(items)
	ids := make([]int64, 0, len(items))
	for _, i := range items {
		ids = append(ids, i.PlatformID)
	}

	store := &model.PlatformStore{Store: s.Store}
	platforms, err := store.FindAll(model.NewPlatformQuery().FindByID(ids...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.Platform, len(platforms))
	for _, i := range platforms {
		idMap[i.ID] = i
	}

	for _, i := range items {
		i.Platform = idMap[i.PlatformID]
	}

	return nil
}

func (s *Builds) IncludeEnv(items ...*model.Build) error {
	items = compactBuilds(items)
	ids := make([]int64, 0, len(items))
	for _, i := range items {
		ids = append(ids, i.EnvID)
	}

	store := &model.EnvStore{Store: s.Store}
	envs, err := store.FindAll(model.NewEnvQuery().FindByID(ids...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.Env, len(envs))
	for _, i := range envs {
		idMap[i.ID] = i
	}

	for _, i := range items {
		i.Env = idMap[i.EnvID]
	}

	return nil
}

func (s *Builds) IncludeRules(items ...*model.Build) error {
	err := s.IncludeReleaseRules(items...)
	if err != nil {
		return err
	}

	err = s.IncludeAtmRules(items...)
	if err != nil {
		return err
	}

	err = s.IncludeNetworkRules(items...)
	if err != nil {
		return err
	}

	return nil
}

func (s *Builds) IncludeReleaseRules(items ...*model.Build) error {
	items = compactBuilds(items)
	ids := make([]interface{}, 0, len(items))
	for _, b := range items {
		b.ReleaseRules = []*model.RuleRelease{}
		ids = append(ids, b.ID)
	}

	ruleStore := model.RuleReleaseStore{Store: s.Store}
	rules, err := ruleStore.FindAll(model.NewRuleReleaseQuery().Where(
		kallax.Or(
			kallax.In(model.Schema.RuleRelease.ClientID, ids...),
			kallax.In(model.Schema.RuleRelease.UtilID, ids...),
			kallax.In(model.Schema.RuleRelease.FirmwareID, ids...),
		),
	))
	if err != nil {
		return err
	}

	buildMap := make(map[int64]*model.Build, len(items))
	for _, i := range items {
		buildMap[i.ID] = i
	}

	for _, r := range rules {
		if r.ClientID != nil && buildMap[*r.ClientID] != nil {
			buildMap[*r.ClientID].ReleaseRules = append(buildMap[*r.ClientID].ReleaseRules, r)
		}
		if r.UtilID != nil && buildMap[*r.UtilID] != nil {
			buildMap[*r.UtilID].ReleaseRules = append(buildMap[*r.UtilID].ReleaseRules, r)
		}
		if r.FirmwareID != nil && buildMap[*r.FirmwareID] != nil {
			buildMap[*r.FirmwareID].ReleaseRules = append(buildMap[*r.FirmwareID].ReleaseRules, r)
		}
	}

	return nil
}

func (s *Builds) IncludeAtmRules(items ...*model.Build) error {
	items = compactBuilds(items)
	ids := make([]interface{}, 0, len(items))
	for _, b := range items {
		b.AtmRules = []*model.RuleAtm{}
		ids = append(ids, b.ID)
	}

	ruleStore := model.RuleAtmStore{Store: s.Store}
	rules, err := ruleStore.FindAll(model.NewRuleAtmQuery().Where(
		kallax.Or(
			kallax.In(model.Schema.RuleAtm.ClientID, ids...),
			kallax.In(model.Schema.RuleAtm.UtilID, ids...),
			kallax.In(model.Schema.RuleAtm.FirmwareID, ids...),
		),
	))
	if err != nil {
		return err
	}

	buildMap := make(map[int64]*model.Build, len(items))
	for _, i := range items {
		buildMap[i.ID] = i
	}

	for _, r := range rules {
		if r.ClientID != nil && buildMap[*r.ClientID] != nil {
			buildMap[*r.ClientID].AtmRules = append(buildMap[*r.ClientID].AtmRules, r)
		}
		if r.UtilID != nil && buildMap[*r.UtilID] != nil {
			buildMap[*r.UtilID].AtmRules = append(buildMap[*r.UtilID].AtmRules, r)
		}
		if r.FirmwareID != nil && buildMap[*r.FirmwareID] != nil {
			buildMap[*r.FirmwareID].AtmRules = append(buildMap[*r.FirmwareID].AtmRules, r)
		}
	}

	return nil
}

func (s *Builds) IncludeNetworkRules(items ...*model.Build) error {
	items = compactBuilds(items)
	ids := make([]interface{}, 0, len(items))
	for _, b := range items {
		b.NetworkRules = []*model.RuleNetwork{}
		ids = append(ids, b.ID)
	}

	ruleStore := model.RuleNetworkStore{Store: s.Store}
	rules, err := ruleStore.FindAll(model.NewRuleNetworkQuery().Where(
		kallax.Or(
			kallax.In(model.Schema.RuleNetwork.ClientID, ids...),
			kallax.In(model.Schema.RuleNetwork.UtilID, ids...),
			kallax.In(model.Schema.RuleNetwork.FirmwareID, ids...),
		),
	))
	if err != nil {
		return err
	}

	buildMap := make(map[int64]*model.Build, len(items))
	for _, i := range items {
		buildMap[i.ID] = i
	}

	for _, r := range rules {
		if r.ClientID != nil && buildMap[*r.ClientID] != nil {
			buildMap[*r.ClientID].NetworkRules = append(buildMap[*r.ClientID].NetworkRules, r)
		}
		if r.UtilID != nil && buildMap[*r.UtilID] != nil {
			buildMap[*r.UtilID].NetworkRules = append(buildMap[*r.UtilID].NetworkRules, r)
		}
		if r.FirmwareID != nil && buildMap[*r.FirmwareID] != nil {
			buildMap[*r.FirmwareID].NetworkRules = append(buildMap[*r.FirmwareID].NetworkRules, r)
		}
	}

	return nil
}

func IncudeBuildURL(root string, items ...*model.Build) {
	for _, i := range items {
		i.IncludeURL(root)
	}
}

func compactBuilds(items []*model.Build) []*model.Build {
	for i := 0; i < len(items); i++ {
		if items[i] == nil {
			items = append(items[:i], items[i+1:]...)
			i--
		}
	}

	return items
}
