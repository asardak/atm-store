package store

import (
	"encoding/json"

	"database/sql"

	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type ActionLogs struct {
	*model.ActionLogStore
}

func NewActionLogs(db *sql.DB) *ActionLogs {
	return &ActionLogs{model.NewActionLogStore(db)}
}

type packageChangesList []*model.PackageChanges

func (l packageChangesList) toPackageList() []*model.Package {
	pp := []*model.Package{}
	for _, c := range l {
		pp = append(pp, &c.Package)
	}

	return pp
}

func (s *ActionLogs) IncludeAll(items ...*model.ActionLog) error {
	changes := packageChangesList{}

	for _, l := range items {
		switch l.Subject {
		case model.RuleReleaseSubject, model.RuleAtmSubject:
			ch := &model.PackageChanges{}
			err := json.Unmarshal([]byte(l.Changes), ch)
			if err != nil {
				return err
			}

			changes = append(changes, ch)
			l.PackageChanges = ch

			ruleStore := Packages{Store: s.Store}
			err = ruleStore.IncludeAll(changes.toPackageList()...)
			if err != nil {
				return err
			}
		}
	}

	return s.IncludeAuthor(items...)
}

func (s *ActionLogs) IncludeAuthor(items ...*model.ActionLog) error {
	ids := make([]int64, 0, len(items))
	for _, i := range items {
		ids = append(ids, i.AuthorID)
	}

	store := &model.UserStore{Store: s.Store}
	rels, err := store.FindAll(model.NewUserQuery().FindByID(ids...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.User, len(rels))
	for _, i := range rels {
		idMap[i.ID] = i
	}

	for _, i := range items {
		i.Author = idMap[i.AuthorID]
	}

	return nil
}

func (s *ActionLogs) FindBySubject(subjectType string, subjectID int64) ([]*model.ActionLog, error) {
	conditions := []kallax.Condition{kallax.Eq(model.Schema.ActionLog.Subject, subjectType)}

	switch subjectType {
	case model.RuleAtmSubject:
		store := model.RuleAtmStore{Store: s.Store}
		rule, err := store.FindOne(model.NewRuleAtmQuery().FindByID(subjectID))
		if err != nil {
			return nil, err
		}

		conditions = append(conditions, kallax.JSONContains(
			model.Schema.ActionLog.Props, map[string]int64{
				"platform_id": rule.PlatformID,
				"env_id":      rule.EnvID,
				"atm_id":      rule.AtmID,
			}),
		)

	case model.RuleReleaseSubject:
		store := model.RuleReleaseStore{Store: s.Store}
		rule, err := store.FindOne(model.NewRuleReleaseQuery().FindByID(subjectID))
		if err != nil {
			return nil, err
		}

		conditions = append(conditions, kallax.JSONContains(
			model.Schema.ActionLog.Props, map[string]int64{
				"platform_id": rule.PlatformID,
				"env_id":      rule.EnvID,
			}),
		)
	}

	q := model.NewActionLogQuery().Where(kallax.And(conditions...)).Order(kallax.Desc(model.Schema.ActionLog.CreatedAt))
	return s.FindAll(q)
}
