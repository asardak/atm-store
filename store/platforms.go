package store

import (
	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type Platforms struct {
	*model.PlatformStore
}

func InitPlatforms(store *kallax.Store) *Platforms {
	return &Platforms{&model.PlatformStore{Store: store}}
}

func (s *Platforms) FindOrCreate(name string) (*model.Platform, error) {
	m, err := s.FindOne(model.NewPlatformQuery().FindByName(name))
	if err == kallax.ErrNotFound {
		m = &model.Platform{Name: name}
		err = s.Insert(m)
	}

	return m, err
}
