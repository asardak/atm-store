package store

import (
	"database/sql"

	"atm-store/model"
	"github.com/AlekSi/pointer"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type RuleReleases struct {
	*model.RuleReleaseStore
}

func NewRuleReleases(db *sql.DB) *RuleReleases {
	return &RuleReleases{model.NewRuleReleaseStore(db)}
}

func (s *RuleReleases) FindPackage(r *model.PackageRequest) (*model.Package, error) {
	if !s.accepted(r) {
		return nil, nil
	}

	q := model.NewRuleReleaseQuery().Where(
		kallax.And(
			kallax.Eq(model.Schema.RuleRelease.PlatformID, r.Platform.ID),
			kallax.Eq(model.Schema.RuleRelease.EnvID, r.Env.ID),
		),
	)
	rule, err := s.FindOne(q)
	if err == kallax.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	rules := &Rules{Store: s.Store}
	err = rules.IncludeAll(rule)
	if err != nil {
		return nil, err
	}

	if rule.Platform.ID != r.Platform.ID {
		return nil, model.ErrWrongPlatform{CorrectPlatform: rule.Platform}
	}
	if rule.Env.ID != r.Env.ID {
		return nil, model.ErrWrongEnv{CorrectEnv: rule.Env}
	}

	return &rule.Package, nil
}

func (s *RuleReleases) IncludeAll(ruleList ...*model.RuleRelease) error {
	genericRules := []model.RuleInterface{}
	for _, r := range ruleList {
		genericRules = append(genericRules, r)
	}

	grs := &Rules{Store: s.Store}
	err := grs.IncludeAll(genericRules...)
	if err != nil {
		return err
	}

	return nil
}

func (s *RuleReleases) LogSave(c getter, r *model.RuleRelease) error {
	var old *model.RuleRelease
	var err error
	actionType := model.UpdateAction
	if r.ID > 0 {
		old, err = s.FindOne(model.NewRuleReleaseQuery().FindByID(r.ID))
		if err != nil {
			return err
		}
	} else {
		actionType = model.CreateAction
	}

	return s.Transaction(func(txStore *model.RuleReleaseStore) error {
		_, err := txStore.Save(r)
		if err != nil {
			return err
		}

		props, err := r.ActionLogProps()
		if err != nil {
			return err
		}

		changes, err := r.BuildChanges(old)
		if err != nil {
			return err
		}

		if changes != nil {
			logStore := model.ActionLogStore{Store: txStore.Store}
			_, err = logStore.Save(&model.ActionLog{
				Subject:    model.RuleReleaseSubject,
				SubjectID:  &r.ID,
				ActionType: actionType,
				Props:      string(props),
				Changes:    string(changes),
				AuthorID:   int64(c.Get("currentUser").(*model.User).ID),
				AuthorIP:   pointer.ToString(c.Get("currentIP").(string)),
			})
			return err
		}

		return nil
	})
}

func (s *RuleReleases) LogDelete(c getter, r *model.RuleRelease) error {
	props, err := r.ActionLogProps()
	if err != nil {
		return err
	}

	return s.Transaction(func(txStore *model.RuleReleaseStore) error {
		err = txStore.Delete(r)
		if err != nil {
			return err
		}

		logStore := model.ActionLogStore{Store: txStore.Store}
		_, err = logStore.Save(&model.ActionLog{
			Subject:    model.RuleReleaseSubject,
			SubjectID:  &r.ID,
			ActionType: model.DeleteAction,
			Changes:    "{}",
			Props:      string(props),
			AuthorID:   int64(c.Get("currentUser").(*model.User).ID),
			AuthorIP:   pointer.ToString(c.Get("currentIP").(string)),
		})

		return err
	})
}

func (s *RuleReleases) accepted(r *model.PackageRequest) bool {
	return r.Platform != nil && r.Env != nil
}
