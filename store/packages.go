package store

import (
	"database/sql"

	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type Packages struct {
	Store *kallax.Store
}

type packageFinder interface {
	FindPackage(request *model.PackageRequest) (*model.Package, error)
}

func NewPackages(db *sql.DB) *Packages {
	return &Packages{Store: kallax.NewStore(db)}
}

func (ps *Packages) FindPackage(request *model.PackageRequest) (*model.Package, error) {
	requests := &PackageRequests{Store: ps.Store}
	err := requests.IncludeAll(request)
	if err != nil {
		return nil, err
	}

	var p *model.Package
	rules := ps.buildFinders()
	for _, r := range rules {
		p, err = r.FindPackage(request)
		if err != nil {
			return nil, err
		}
		if p != nil {
			break
		}
	}

	if p == nil {
		p = &model.Package{}
	}
	if p.Completed() {
		return p, nil
	}

	releases := &RuleReleases{&model.RuleReleaseStore{Store: ps.Store}}
	rp, err := releases.FindPackage(request)
	if err != nil {
		return nil, err
	}

	p.Merge(rp)
	return p, nil
}

func (ps *Packages) buildFinders() []packageFinder {
	return []packageFinder{
		InitRuleAtms(ps.Store),
		InitRuleNetworks(ps.Store),
	}
}

func (ps *Packages) IncludeAll(items ...*model.Package) error {
	buildIDs := make([]int64, 0, len(items))
	for _, p := range items {
		for _, bid := range []*int64{p.ClientID, p.UtilID, p.FirmwareID} {
			if bid != nil {
				buildIDs = append(buildIDs, *bid)
			}
		}
	}

	store := &model.BuildStore{Store: ps.Store}
	builds, err := store.FindAll(model.NewBuildQuery().FindByID(buildIDs...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.Build, len(builds))
	for _, i := range builds {
		idMap[i.ID] = i
	}

	for _, p := range items {
		if !p.NoClient && p.ClientID != nil {
			p.Client = idMap[*p.ClientID]
		}
		if !p.NoUtil && p.UtilID != nil {
			p.Util = idMap[*p.UtilID]
		}
		if !p.NoFirmware && p.FirmwareID != nil {
			p.Firmware = idMap[*p.FirmwareID]
		}
	}

	return nil
}
