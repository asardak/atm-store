package store

import (
	"database/sql"

	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type PackageRequests struct {
	Store *kallax.Store
}

func NewPackageRequestStore(db *sql.DB) *PackageRequests {
	return &PackageRequests{Store: kallax.NewStore(db)}
}

func (s *PackageRequests) IncludeAll(r *model.PackageRequest) error {
	if r.Atm == nil && r.SN != "" {
		atms := &model.AtmStore{Store: s.Store}
		atm, err := atms.FindOne(model.NewAtmQuery().FindBySN(r.SN))
		if err != nil && err != kallax.ErrNotFound {
			return err
		}
		r.Atm = atm
	}

	if r.Platform == nil {
		platforms := &model.PlatformStore{Store: s.Store}
		platform, err := platforms.FindOne(model.NewPlatformQuery().FindByName(r.PlatformName))
		if err != nil {
			return err
		}
		r.Platform = platform
	}

	if r.Env == nil {
		envs := &model.EnvStore{Store: s.Store}
		env, err := envs.FindOne(model.NewEnvQuery().FindByName(r.EnvName))
		if err != nil {
			return err
		}
		r.Env = env
	}

	return nil
}
