package store

import (
	"database/sql"

	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type Rules struct {
	Store *kallax.Store
}

func NewRuleGenericStore(db *sql.DB) *Rules {
	return &Rules{Store: kallax.NewStore(db)}
}

func (s *Rules) IncludeAll(items ...model.RuleInterface) error {
	err := s.IncludePlatform(items...)
	if err != nil {
		return err
	}

	err = s.IncludeEnv(items...)
	if err != nil {
		return err
	}

	err = s.IncludePackage(items...)
	if err != nil {
		return err
	}

	return nil
}

func (s *Rules) IncludePlatform(items ...model.RuleInterface) error {
	ids := make([]int64, 0, len(items))
	for _, i := range items {
		i := i.GetRule()
		ids = append(ids, i.PlatformID)
	}

	store := &model.PlatformStore{Store: s.Store}
	rels, err := store.FindAll(model.NewPlatformQuery().FindByID(ids...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.Platform, len(rels))
	for _, i := range rels {
		idMap[i.ID] = i
	}

	for _, i := range items {
		i := i.GetRule()
		i.Platform = idMap[i.PlatformID]
	}

	return nil
}

func (s *Rules) IncludeEnv(items ...model.RuleInterface) error {
	ids := make([]int64, 0, len(items))
	for _, i := range items {
		i := i.GetRule()
		ids = append(ids, i.EnvID)
	}

	store := &model.EnvStore{Store: s.Store}
	rels, err := store.FindAll(model.NewEnvQuery().FindByID(ids...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.Env, len(rels))
	for _, i := range rels {
		idMap[i.ID] = i
	}

	for _, i := range items {
		i := i.GetRule()
		i.Env = idMap[i.EnvID]
	}

	return nil
}

func (s *Rules) IncludePackage(items ...model.RuleInterface) error {
	packages := []*model.Package{}
	for _, rule := range items {
		rule := rule.GetRule()
		packages = append(packages, &rule.Package)
	}

	ps := &Packages{Store: s.Store}
	return ps.IncludeAll(packages...)
}
