package store

import (
	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type Envs struct {
	*model.EnvStore
}

func InitEnvs(store *kallax.Store) *Envs {
	return &Envs{&model.EnvStore{Store: store}}
}

func (s *Envs) FindOrCreate(name string) (*model.Env, error) {
	p, err := s.FindOne(model.NewEnvQuery().FindByName(name))
	if err == kallax.ErrNotFound {
		p = &model.Env{Name: name}
		err = s.Insert(p)
	}

	return p, err
}
