package store

import (
	"database/sql"

	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type Networks struct {
	*model.NetworkStore
}

func NewNetworks(db *sql.DB) *Networks {
	return &Networks{NetworkStore: model.NewNetworkStore(db)}
}

func (s *Networks) FindByIP(ip string) (*model.Network, error) {
	q := `SELECT id, description, addr FROM network WHERE addr >>= $1 ORDER BY addr DESC LIMIT 1`
	set, err := s.RawQuery(q, ip)
	if err != nil {
		return nil, err
	}
	defer set.Close()

	if set.Next() {
		network := &model.Network{}
		var desc *string

		err = set.RawScan(&network.ID, &desc, &network.Addr)
		if err != nil {
			return nil, err
		}
		if desc != nil {
			network.Description = desc
		}
		return network, nil
	}

	return nil, kallax.ErrNotFound
}
