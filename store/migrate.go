package store

import (
	"database/sql"

	"github.com/pkg/errors"
	"github.com/restream/migration"
)

var migrations = []migration.Migration{
	m{
		name: "001_init",
		sql:  initMigration,
	},
}

// m - структура миграции, реализует интерфейс migration.Migration
type m struct {
	name, sql string
}

// Apply выполняет Query-комманду миграции
func (m m) Apply(tx *sql.Tx) error {
	_, err := tx.Exec(m.sql)
	return err
}

// Rollback откатывает миграцию (не используется)
func (m m) Rollback(tx *sql.Tx) error { return nil }

// Name возвращает имя миграции
func (m m) Name() string { return m.name }

// Migrate - запускает миграцию схемы БД
func Migrate(db *sql.DB) error {
	sch := migration.NewSchema(db, migration.DefaultSchemaName, migration.DefaultMigrationTableName)
	err := sch.Init()
	if err != nil {
		return errors.Wrap(err, "(*migration.Schema).Init() failed")
	}

	migs, err := sch.FindUnapplied(migrations)
	if err != nil {
		return errors.Wrap(err, "(*migration.Schema).FindUnapplied() failed")
	}

	_, err = sch.ApplyEach(migs)
	return errors.Wrap(err, "(*migration.Schema).ApplyEach() failed")
}
