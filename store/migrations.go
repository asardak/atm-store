package store

var initMigration = `
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: citext; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: action_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE action_log (
    id integer NOT NULL,
    subject text NOT NULL,
    subject_id integer,
    action_type text NOT NULL,
    props jsonb,
    changes jsonb,
    author_id integer NOT NULL,
    author_ip inet,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: action_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE action_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: action_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE action_log_id_seq OWNED BY action_log.id;


--
-- Name: builds; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE builds (
    id integer NOT NULL,
    version text NOT NULL,
    version_num integer NOT NULL,
    version_id text,
    component text NOT NULL,
    md5 text,
    size integer DEFAULT 0 NOT NULL,
    file_name text NOT NULL,
    description text,
    force_downgrade boolean DEFAULT false NOT NULL,
    platform_id integer NOT NULL,
    env_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: builds_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE builds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: builds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE builds_id_seq OWNED BY builds.id;


--
-- Name: atm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE atm (
    id integer NOT NULL,
    sn text NOT NULL,
    ip inet,
    mac macaddr,
    platform_id integer,
    env_id integer,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: atm_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE atm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: atm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE atm_id_seq OWNED BY atm.id;


--
-- Name: rule_atm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE rule_atm (
    id integer NOT NULL,
    client_id integer,
    util_id integer,
    firmware_id integer,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    no_client boolean DEFAULT false NOT NULL,
    no_firmware boolean DEFAULT false NOT NULL,
    no_util boolean DEFAULT false NOT NULL,
    platform_id integer NOT NULL,
    env_id integer NOT NULL,
    comment text DEFAULT ''::text,
    atm_id integer NOT NULL
);


--
-- Name: atms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE atms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: atms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE atms_id_seq OWNED BY rule_atm.id;


--
-- Name: envs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE envs (
    id integer NOT NULL,
    name text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: envs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE envs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: envs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE envs_id_seq OWNED BY envs.id;


--
-- Name: network; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE network (
    id integer NOT NULL,
    addr inet NOT NULL,
    description text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: network_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE network_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: network_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE network_id_seq OWNED BY network.id;


--
-- Name: platforms; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE platforms (
    id integer NOT NULL,
    name text NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: platforms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE platforms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: platforms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE platforms_id_seq OWNED BY platforms.id;


--
-- Name: releases; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE releases (
    id integer NOT NULL,
    build_id integer NOT NULL,
    component text NOT NULL,
    platform_id integer NOT NULL,
    env_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    author_id integer,
    author_ip text DEFAULT ''::text NOT NULL
);


--
-- Name: releases_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE releases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: releases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE releases_id_seq OWNED BY releases.id;


--
-- Name: rule_network; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE rule_network (
    id integer NOT NULL,
    platform_id integer NOT NULL,
    env_id integer NOT NULL,
    network_id integer NOT NULL,
    client_id integer,
    util_id integer,
    firmware_id integer,
    no_client boolean DEFAULT false NOT NULL,
    no_util boolean DEFAULT false NOT NULL,
    no_firmware boolean DEFAULT false NOT NULL,
    comment text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: rule_network_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE rule_network_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rule_network_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE rule_network_id_seq OWNED BY rule_network.id;


--
-- Name: rule_release; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE rule_release (
    id integer NOT NULL,
    platform_id integer NOT NULL,
    env_id integer NOT NULL,
    client_id integer,
    util_id integer,
    firmware_id integer,
    no_client boolean DEFAULT false NOT NULL,
    no_util boolean DEFAULT false NOT NULL,
    no_firmware boolean DEFAULT false NOT NULL,
    comment text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


--
-- Name: rule_release_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE rule_release_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rule_release_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE rule_release_id_seq OWNED BY rule_release.id;

--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE users (
    id integer NOT NULL,
    login text,
    role integer,
    password_hash text,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL,
    deleted_at timestamp without time zone
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: action_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY action_log ALTER COLUMN id SET DEFAULT nextval('action_log_id_seq'::regclass);


--
-- Name: builds id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY builds ALTER COLUMN id SET DEFAULT nextval('builds_id_seq'::regclass);


--
-- Name: atm id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY atm ALTER COLUMN id SET DEFAULT nextval('atm_id_seq'::regclass);


--
-- Name: envs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY envs ALTER COLUMN id SET DEFAULT nextval('envs_id_seq'::regclass);


--
-- Name: network id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY network ALTER COLUMN id SET DEFAULT nextval('network_id_seq'::regclass);


--
-- Name: platforms id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY platforms ALTER COLUMN id SET DEFAULT nextval('platforms_id_seq'::regclass);


--
-- Name: releases id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY releases ALTER COLUMN id SET DEFAULT nextval('releases_id_seq'::regclass);


--
-- Name: rule_atm id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm ALTER COLUMN id SET DEFAULT nextval('atms_id_seq'::regclass);


--
-- Name: rule_network id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network ALTER COLUMN id SET DEFAULT nextval('rule_network_id_seq'::regclass);


--
-- Name: rule_release id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_release ALTER COLUMN id SET DEFAULT nextval('rule_release_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: action_log action_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY action_log
    ADD CONSTRAINT action_log_pkey PRIMARY KEY (id);


--
-- Name: builds builds_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_pkey PRIMARY KEY (id);


--
-- Name: builds builds_platform_id_env_id_component_version_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_platform_id_env_id_component_version_key UNIQUE (platform_id, env_id, component, version);


--
-- Name: atm atm_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY atm
    ADD CONSTRAINT atm_pkey PRIMARY KEY (id);


--
-- Name: atm atm_sn_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY atm
    ADD CONSTRAINT atm_sn_key UNIQUE (sn);


--
-- Name: rule_atm atms_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm
    ADD CONSTRAINT atms_pkey PRIMARY KEY (id);


--
-- Name: envs envs_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY envs
    ADD CONSTRAINT envs_name_key UNIQUE (name);


--
-- Name: envs envs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY envs
    ADD CONSTRAINT envs_pkey PRIMARY KEY (id);


--
-- Name: network network_addr_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_addr_key UNIQUE (addr);


--
-- Name: network network_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY network
    ADD CONSTRAINT network_pkey PRIMARY KEY (id);


--
-- Name: platforms platforms_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY platforms
    ADD CONSTRAINT platforms_name_key UNIQUE (name);


--
-- Name: platforms platforms_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY platforms
    ADD CONSTRAINT platforms_pkey PRIMARY KEY (id);


--
-- Name: releases releases_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY releases
    ADD CONSTRAINT releases_pkey PRIMARY KEY (id);


--
-- Name: rule_network rule_network_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network
    ADD CONSTRAINT rule_network_pkey PRIMARY KEY (id);


--
-- Name: rule_release rule_release_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_release
    ADD CONSTRAINT rule_release_pkey PRIMARY KEY (id);


--
-- Name: users users_login_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_login_key UNIQUE (login);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: action_log_author_id_subject_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX action_log_author_id_subject_idx ON action_log USING btree (author_id, subject);


--
-- Name: action_log_props_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX action_log_props_idx ON action_log USING gin (props);


--
-- Name: action_log_subject_subject_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX action_log_subject_subject_id_idx ON action_log USING btree (subject, subject_id);


--
-- Name: builds_component_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_component_idx ON builds USING btree (component);


--
-- Name: builds_env_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_env_id_idx ON builds USING btree (env_id);


--
-- Name: builds_platform_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_platform_id_idx ON builds USING btree (platform_id);


--
-- Name: builds_version_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX builds_version_idx ON builds USING btree (version_num);


--
-- Name: atm_platform_id_env_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX atm_platform_id_env_id_idx ON atm USING btree (platform_id, env_id);


--
-- Name: releases_build_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX releases_build_id_idx ON releases USING btree (build_id);


--
-- Name: releases_component_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX releases_component_idx ON releases USING btree (component);


--
-- Name: releases_env_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX releases_env_id_idx ON releases USING btree (env_id);


--
-- Name: releases_platform_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX releases_platform_id_idx ON releases USING btree (platform_id);


--
-- Name: rule_atm_atm_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX rule_atm_atm_id_idx ON rule_atm USING btree (atm_id);


--
-- Name: rule_network_network_id_platform_id_env_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX rule_network_network_id_platform_id_env_id_idx ON rule_network USING btree (network_id, platform_id, env_id);


--
-- Name: rule_release_platform_id_env_id_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX rule_release_platform_id_env_id_idx ON rule_release USING btree (platform_id, env_id);


--
-- Name: action_log action_log_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY action_log
    ADD CONSTRAINT action_log_author_id_fkey FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: builds builds_env_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_env_id_fkey FOREIGN KEY (env_id) REFERENCES envs(id);


--
-- Name: builds builds_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY builds
    ADD CONSTRAINT builds_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES platforms(id);


--
-- Name: atm atm_env_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY atm
    ADD CONSTRAINT atm_env_id_fkey FOREIGN KEY (env_id) REFERENCES envs(id);


--
-- Name: atm atm_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY atm
    ADD CONSTRAINT atm_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES platforms(id);


--
-- Name: rule_atm atms_client_build_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm
    ADD CONSTRAINT atms_client_build_id_fkey FOREIGN KEY (client_id) REFERENCES builds(id);


--
-- Name: rule_atm atms_env_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm
    ADD CONSTRAINT atms_env_id_fkey FOREIGN KEY (env_id) REFERENCES envs(id);


--
-- Name: rule_atm atms_firmware_build_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm
    ADD CONSTRAINT atms_firmware_build_id_fkey FOREIGN KEY (firmware_id) REFERENCES builds(id);


--
-- Name: rule_atm atms_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm
    ADD CONSTRAINT atms_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES platforms(id);


--
-- Name: rule_atm atms_util_build_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm
    ADD CONSTRAINT atms_util_build_id_fkey FOREIGN KEY (util_id) REFERENCES builds(id);


--
-- Name: releases releases_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY releases
    ADD CONSTRAINT releases_author_id_fkey FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: releases releases_env_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY releases
    ADD CONSTRAINT releases_env_id_fkey FOREIGN KEY (env_id) REFERENCES envs(id);


--
-- Name: releases releases_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY releases
    ADD CONSTRAINT releases_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES platforms(id);


--
-- Name: rule_atm rule_atm_atm_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_atm
    ADD CONSTRAINT rule_atm_atm_id_fkey FOREIGN KEY (atm_id) REFERENCES atm(id);


--
-- Name: rule_network rule_network_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network
    ADD CONSTRAINT rule_network_client_id_fkey FOREIGN KEY (client_id) REFERENCES builds(id);


--
-- Name: rule_network rule_network_env_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network
    ADD CONSTRAINT rule_network_env_id_fkey FOREIGN KEY (env_id) REFERENCES envs(id);


--
-- Name: rule_network rule_network_firmware_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network
    ADD CONSTRAINT rule_network_firmware_id_fkey FOREIGN KEY (firmware_id) REFERENCES builds(id);


--
-- Name: rule_network rule_network_network_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network
    ADD CONSTRAINT rule_network_network_id_fkey FOREIGN KEY (network_id) REFERENCES network(id);


--
-- Name: rule_network rule_network_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network
    ADD CONSTRAINT rule_network_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES platforms(id);


--
-- Name: rule_network rule_network_util_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_network
    ADD CONSTRAINT rule_network_util_id_fkey FOREIGN KEY (util_id) REFERENCES builds(id);


--
-- Name: rule_release rule_release_client_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_release
    ADD CONSTRAINT rule_release_client_id_fkey FOREIGN KEY (client_id) REFERENCES builds(id);


--
-- Name: rule_release rule_release_env_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_release
    ADD CONSTRAINT rule_release_env_id_fkey FOREIGN KEY (env_id) REFERENCES envs(id);


--
-- Name: rule_release rule_release_firmware_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_release
    ADD CONSTRAINT rule_release_firmware_id_fkey FOREIGN KEY (firmware_id) REFERENCES builds(id);


--
-- Name: rule_release rule_release_platform_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_release
    ADD CONSTRAINT rule_release_platform_id_fkey FOREIGN KEY (platform_id) REFERENCES platforms(id);


--
-- Name: rule_release rule_release_util_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY rule_release
    ADD CONSTRAINT rule_release_util_id_fkey FOREIGN KEY (util_id) REFERENCES builds(id);


--
-- PostgreSQL database dump complete
--
`
