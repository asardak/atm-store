package store

import (
	"database/sql"

	"atm-store/model"
	"github.com/AlekSi/pointer"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type RuleAtms struct {
	*model.RuleAtmStore
}

func NewRuleAtms(db *sql.DB) *RuleAtms {
	return &RuleAtms{model.NewRuleAtmStore(db)}
}

func InitRuleAtms(store *kallax.Store) *RuleAtms {
	return &RuleAtms{&model.RuleAtmStore{Store: store}}
}

func (s *RuleAtms) FindPackage(r *model.PackageRequest) (*model.Package, error) {
	if !s.accepted(r) {
		return nil, nil
	}

	q := model.NewRuleAtmQuery().FindByAtmID(kallax.Eq, r.Atm.ID)
	rule, err := s.FindOne(q)
	if err == kallax.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	rules := &Rules{Store: s.Store}
	err = rules.IncludeAll(rule)
	if err != nil {
		return nil, err
	}

	if rule.Platform.ID != r.Platform.ID {
		return nil, model.ErrWrongPlatform{CorrectPlatform: rule.Platform}
	}
	if rule.Env.ID != r.Env.ID {
		return nil, model.ErrWrongEnv{CorrectEnv: rule.Env}
	}

	return &rule.Package, nil
}

func (s *RuleAtms) IncludeAll(ruleList ...*model.RuleAtm) error {
	genericRules := []model.RuleInterface{}
	for _, r := range ruleList {
		genericRules = append(genericRules, r)
	}

	grs := &Rules{Store: s.Store}
	err := grs.IncludeAll(genericRules...)
	if err != nil {
		return err
	}

	err = s.IncludeAtm(ruleList...)
	if err != nil {
		return err
	}

	return nil
}

func (s *RuleAtms) IncludeAtm(ruleList ...*model.RuleAtm) error {
	atmIDs := make([]int64, 0, len(ruleList))
	for _, rule := range ruleList {
		atmIDs = append(atmIDs, rule.AtmID)
	}

	store := &model.AtmStore{Store: s.Store}
	atms, err := store.FindAll(model.NewAtmQuery().FindByID(atmIDs...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.Atm, len(atms))
	for _, i := range atms {
		idMap[i.ID] = i
	}

	for _, rule := range ruleList {
		rule.Atm = idMap[rule.AtmID]
	}

	return nil
}

func (s *RuleAtms) accepted(r *model.PackageRequest) bool {
	return r.Platform != nil && r.Env != nil && r.Atm != nil
}

type getter interface {
	Get(key string) interface{}
}

func (s *RuleAtms) LogSave(c getter, r *model.RuleAtm) error {
	var old *model.RuleAtm
	var err error
	actionType := model.UpdateAction
	if r.ID > 0 {
		old, err = s.FindOne(model.NewRuleAtmQuery().FindByID(r.ID))
		if err != nil {
			return err
		}
	} else {
		actionType = model.CreateAction
	}

	return s.Transaction(func(txStore *model.RuleAtmStore) error {
		_, err := txStore.Save(r)
		if err != nil {
			return err
		}

		props, err := r.ActionLogProps()
		if err != nil {
			return err
		}

		changes, err := r.BuildChanges(old)
		if err != nil {
			return err
		}

		if changes != nil {
			logStore := model.ActionLogStore{Store: txStore.Store}
			_, err = logStore.Save(&model.ActionLog{
				Subject:    model.RuleAtmSubject,
				SubjectID:  &r.ID,
				ActionType: actionType,
				Props:      string(props),
				Changes:    string(changes),
				AuthorID:   int64(c.Get("currentUser").(*model.User).ID),
				AuthorIP:   pointer.ToString(c.Get("currentIP").(string)),
			})
			return err
		}

		return nil
	})
}

func (s *RuleAtms) LogDelete(c getter, r *model.RuleAtm) error {
	props, err := r.ActionLogProps()
	if err != nil {
		return err
	}

	return s.Transaction(func(txStore *model.RuleAtmStore) error {
		err = txStore.Delete(r)
		if err != nil {
			return err
		}

		logStore := model.ActionLogStore{Store: txStore.Store}
		_, err = logStore.Save(&model.ActionLog{
			Subject:    model.RuleAtmSubject,
			SubjectID:  &r.ID,
			ActionType: model.DeleteAction,
			Changes:    "{}",
			Props:      string(props),
			AuthorID:   int64(c.Get("currentUser").(*model.User).ID),
			AuthorIP:   pointer.ToString(c.Get("currentIP").(string)),
		})

		return err
	})
}
