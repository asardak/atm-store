package store

import (
	"database/sql"

	"atm-store/model"
	kallax "gopkg.in/src-d/go-kallax.v1"
)

type RuleNetworks struct {
	*model.RuleNetworkStore
}

func NewRuleNetworks(db *sql.DB) *RuleNetworks {
	return &RuleNetworks{model.NewRuleNetworkStore(db)}
}

func InitRuleNetworks(store *kallax.Store) *RuleNetworks {
	return &RuleNetworks{&model.RuleNetworkStore{Store: store}}
}

func (s *RuleNetworks) FindPackage(r *model.PackageRequest) (*model.Package, error) {
	netStore := &Networks{&model.NetworkStore{s.Store}}
	net, err := netStore.FindByIP(r.IP)
	if err == kallax.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	q := model.NewRuleNetworkQuery().Where(
		kallax.And(
			kallax.Eq(model.Schema.RuleNetwork.PlatformID, r.Platform.ID),
			kallax.Eq(model.Schema.RuleNetwork.NetworkID, net.ID),
		),
	)
	rules, err := s.FindAll(q)
	if err == kallax.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	err = s.IncludeRule(rules...)
	if err != nil {
		return nil, err
	}

	for _, rule := range rules {
		if rule.EnvID == r.Env.ID {
			return &rule.Package, nil
		}
	}

	return nil, nil
}

func (s *RuleNetworks) IncludeAll(rules ...*model.RuleNetwork) error {
	err := s.IncludeRule(rules...)
	if err != nil {
		return err
	}

	err = s.IncludeNetwork(rules...)
	if err != nil {
		return err
	}

	return nil
}

func (s *RuleNetworks) IncludeRule(rules ...*model.RuleNetwork) error {
	genericRules := []model.RuleInterface{}
	for _, r := range rules {
		genericRules = append(genericRules, r)
	}

	grs := &Rules{Store: s.Store}
	return grs.IncludeAll(genericRules...)
}

func (s *RuleNetworks) IncludeNetwork(rules ...*model.RuleNetwork) error {
	ids := make([]int64, 0, len(rules))
	for _, i := range rules {
		ids = append(ids, i.NetworkID)
	}

	store := &model.NetworkStore{Store: s.Store}
	rels, err := store.FindAll(model.NewNetworkQuery().FindByID(ids...))
	if err != nil {
		return err
	}

	idMap := make(map[int64]*model.Network, len(rels))
	for _, i := range rels {
		idMap[i.ID] = i
	}

	for _, i := range rules {
		i.Network = idMap[i.NetworkID]
	}

	return nil
}
