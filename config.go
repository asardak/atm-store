package atmstore

type Config struct {
	APIAddr string `json:"api_addr"`

	Environment string `json:"environment"`
	BinRoot     string `json:"bin_root"`
	PublicRoot  string `json:"public_root"`

	HashKey  string `json:"hash_key"`
	BlockKey string `json:"block_key"`

	Postgres struct {
		URL          string `json:"url"`
		MaxOpenConns int    `json:"max_open_conns"`
	} `json:"postgres"`
}
