package main

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"atm-store"
	"atm-store/api"
	"atm-store/store"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	_ "github.com/restream/reindexer/bindings/cproto"
)

type flags struct {
	version bool
	config  string
	migrate bool
	seed    bool
}

func main() {
	time.Local = time.UTC

	f := parseFlags()
	if f.version {
		fmt.Println(atmstore.Version)
		return
	}

	cfg, err := loadConfig(f.config)
	if err != nil {
		log.Fatalln(err)
	}

	if f.seed {
		err = store.Seed(cfg)
		if err != nil {
			log.Fatalln(err)
		}
		return
	}

	db, err := openDB(cfg)
	if err != nil {
		log.Fatalln(err)
	}

	if f.migrate {
		err = store.Migrate(db)
		if err != nil {
			log.Fatalln(err)
		}
		return
	}

	hashKey, blockKey, err := getCookieKeys(cfg)
	if err != nil {
		log.Fatal(err)
	}

	api := api.New(db, cfg.PublicRoot, cfg.BinRoot, hashKey, blockKey)
	apiAddr := getApiAddr(cfg)
	log.Fatalln(api.Listen(apiAddr))
}

func parseFlags() *flags {
	f := &flags{}

	flag.BoolVar(&f.version, "version", false, "Show version")
	flag.StringVar(&f.config, "config", "config.json", "Path to config file")
	flag.BoolVar(&f.migrate, "migrate", false, "Run migrations")
	flag.BoolVar(&f.seed, "seed", false, "Bootstrap database")
	flag.Parse()

	return f
}

func loadConfig(path string) (*atmstore.Config, error) {
	cfg := &atmstore.Config{}

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, errors.Wrap(err, "loadConfig() failed")
	}

	err = json.Unmarshal(data, cfg)
	return cfg, errors.Wrap(err, "loadConfig() failed")
}

func openDB(cfg *atmstore.Config) (*sql.DB, error) {
	db, err := sql.Open("postgres", cfg.Postgres.URL)
	if err != nil {
		return nil, errors.Wrap(err, "sql.Open() failed")
	}

	err = db.Ping()
	if err != nil {
		return nil, errors.Wrap(err, "(*sql.DB).Ping() failed")
	}

	db.SetMaxOpenConns(cfg.Postgres.MaxOpenConns)
	db.SetMaxIdleConns(cfg.Postgres.MaxOpenConns / 2)

	return db, nil
}

func getCookieKeys(cfg *atmstore.Config) ([]byte, []byte, error) {
	bHashKey, err := base64.StdEncoding.DecodeString(cfg.HashKey)
	if err != nil {
		return nil, nil, err
	}

	bBlockKey, err := base64.StdEncoding.DecodeString(cfg.BlockKey)
	if err != nil {
		return nil, nil, err
	}

	return bHashKey, bBlockKey, nil
}

func getApiAddr(cfg *atmstore.Config) string {
	const API_HOST = "API_HOST"
	const API_PORT = "API_PORT"

	env_host := os.Getenv(API_HOST)
	env_port := os.Getenv(API_PORT)

	if len(env_host) > 0 && len(env_port) > 0 {
		return env_host + ":" + env_port
	}

	return cfg.APIAddr
}
