ENV := ""
GO_PACKAGE := atm-store
CMD := atm-store
OUT := bin/$(CMD)
PKGS := $$(go list ./... | grep -v vendor)

build:
	-rm $(OUT)
	go build -i -ldflags "-X $(GO_PACKAGE).Version=1.0.0" -o $(OUT) ./cmd/$(CMD)
	rice -i ./api append --exec=$(OUT)

gen:
	go generate ./...

run: build
	$(OUT)

db: build
	$(OUT) -migrate

seed: build
	$(OUT) -seed

start: seed run

BUILD_TS := $(shell date +"%Y%m%d%H%M%S")
front:
	rm -rf ./spa/dist/
	cd spa && npm i && npm run build
	env TS=$(BUILD_TS) ./spa/src/index.html.sh > ./spa/dist/index.html
	mv ./spa/dist/build.js ./spa/dist/build-$(BUILD_TS).js
	cp ./spa/icon.css ./spa/dist/icon.css
	cp ./spa/font.woff2 ./spa/dist/font.woff2

fun: front run
