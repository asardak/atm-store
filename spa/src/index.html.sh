#!/usr/bin/env bash

cat <<-EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ATMStore</title>
        <link href="/dist/icon.css?family=Material+Icons" rel="stylesheet">
    </head>
    <body>
        <div id="app"></div>
        <script src="/dist/build-$TS.js"></script>
    </body>
</html>
EOF