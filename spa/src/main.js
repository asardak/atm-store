import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router.js'
import Buefy from 'buefy'
import VeeValidate from 'vee-validate';
import axios from 'axios';
import VueAxios from 'vue-axios';
import sessionMan from './session_man.vue'
import moment from 'moment';

import 'buefy/lib/buefy.css'
import 'font-awesome/css/font-awesome.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';

moment.locale("ru");

Vue.use(VueRouter);
Vue.use(Buefy);
Vue.use(VeeValidate, { inject: false });
Vue.use(VueAxios, axios);

Vue.filter("upcase", (v) => {
    return v.toUpperCase()
});

Vue.filter("date", (v) => {
    if (typeof v === "number") {
        v = new Date(v*1000)
    }

    return moment(v).format("D MMMM Y, HH:mm");
});

Vue.filter("bytes", (v) => {
    var i = Math.floor(Math.log(v) / Math.log(1024));
    return (v / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
});

Vue.filter("json", (v) => {
    if (typeof v === "string") {
        v = JSON.parse(v);
    }

    return JSON.stringify(v, null, 4);
});

Vue.mixin({
    methods: {
        notifyError(err, msg) {
            console.error("error:", err);
            this.$toast.open({
                message: msg,
                type: 'is-danger'
            });
        },
        notifySuccess(msg) {
            this.$toast.open({
                message: msg,
                type: 'is-success'
            });
        },
        signIn() {
            return this.$root.sessionMan.openSignIn();
        },
        signOut() {
            this.$root.sessionMan.signOut();
        },
        currentUser() {
            return this.$root.sessionMan.currentUser;
        },
        mergeQuery(params) {
            var query = Object.assign({}, this.$route.query);
            Object.entries(query).forEach( o => (params[o[0]] === null ? delete query[o[0]] : 0));
            Object.entries(params).forEach( o => (o[1] === null ? delete params[o[0]] : 0));

            return Object.assign(query, params);
        },
    },
});

new Vue({
    el: '#app',
    router,
    render: h => h(App),
    mounted(){
        this.$el.appendChild(this.sessionMan.$el);
    },
    computed: {
        sessionMan() {
            var c = Vue.extend(sessionMan);
            return (new c()).$mount();
        },
    },
});
