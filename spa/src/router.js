import VueRouter from 'vue-router';
import RuleSubmenu from './rule/submenu.vue';
import ComponentSubmenu from './component/submenu.vue';
import RuleAtm from './rule_atm/index.vue';
import RuleAtmShow from './rule_atm/show.vue';
import RuleRelease from './rule_release/index.vue';
import RuleReleaseShow from './rule_release/show.vue';
import RuleNetwork from './rule_network/index.vue';
import RuleNetworkShow from './rule_network/show.vue';
import BuildIndex from './build/index.vue';
import BuildShow from './build/show.vue';
import UserIndex from './user/index.vue';

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/face/strategy',
            redirect: '/face/strategy/release'
        },
        {
            path: '/face/strategy/release',
            components: {
                default: RuleRelease,
                submenu: RuleSubmenu,
            },
        },
        {
            path: '/face/strategy/release/rule/:rule_id',
            components: {
                default: RuleReleaseShow,
                submenu: RuleSubmenu,
            },
            props: { default: true, sidebar: false }
        },
        {
            path: '/face/component',
            redirect: '/face/component/build'
        },
        {
            path: '/face/strategy/atm',
            components: {
                default: RuleAtm,
                submenu: RuleSubmenu,
            },
        },
        {
            path: '/face/strategy/atm/rule/:rule_id',
            components: {
                default: RuleAtmShow,
                submenu: RuleSubmenu,
            },
            props: { default: true, submenu: false }
        },
        {
            path: '/face/strategy/network',
            components: {
                default: RuleNetwork,
                submenu: RuleSubmenu,
            },

        },
        {
            path: '/face/strategy/network/:network_id',
            components: {
                default: RuleNetwork,
                submenu: RuleSubmenu,
            },
            props: { default: true, submenu: false }
        },
        {
            path: '/face/strategy/network/rule/:rule_id',
            components: {
                default: RuleNetworkShow,
                submenu: RuleSubmenu,
            },
            props: { default: true, submenu: false }
        },
        {
            path: '/face/component/build',
            components: {
                default: BuildIndex,
                submenu: ComponentSubmenu,
            },
        },
        {
            path: '/face/component/build/:build_id',
            components: {
                default: BuildShow,
                submenu: ComponentSubmenu,
            },
            props: { default: true, submenu: false }
        },
        {
            path: '/face/users',
            component: UserIndex,
        },
    ]
});